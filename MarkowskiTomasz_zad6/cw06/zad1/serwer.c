#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/msg.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<signal.h>
#include<time.h>
#include "komunikaty.h"

struct client{
    key_t key;
    int msg;
    int friends[15];
    int friends_no;
    int connected;
};

struct client clients[100];
int clients_no = 0;

void addFriend(struct client *client, int friend){
    for(int i=0; i<client->friends_no; i++){
        if(client->friends[i] == friend) return; //already a friend
    }
    client->friends[client->friends_no++] = friend;
}

void deleteFriend(struct client *client, int friend){
    for(int i=0; i<client->friends_no; i++){
        if(client->friends[i] == friend){
            for(int j=i; j<client->friends_no-1; j++){
                client->friends[j] = client->friends[j+1];
            }
            client->friends_no--;

            return;
        }
    }
}

int main_queue;

void sigintHandler(int signo){
    msgctl(main_queue, IPC_RMID, NULL);

    /* informowanie klientów */
    struct SB_INT sb_int;
    sb_int.mtype = CSTOP;

    for(int i=0; i<clients_no; i++){
        if(clients[i].connected == 1){
            msgsnd(clients[i].msg, &sb_int, sizeof(int), 0);
        }
    }

    exit(0);
}

int main(int argc, char *argv[]){
    signal(SIGINT, sigintHandler);
    char *path = getenv("HOME");
    key_t main_key = ftok(path, 'a');
    printf("generated key: %d\n", main_key);
    main_queue = msgget(main_key, IPC_CREAT|0666);
    if(main_queue == -1){ 
        perror("Cannot create queue");
        exit(1);
    }


    struct SB_TEXT msg_buff;
    //msg_buff.mtype = 1;
    //msg_buff.mtext = calloc(100, sizeof(char));
    struct SB_INT *rel;
    time_t now;
    static char no[5];
    while(msgrcv(main_queue, &msg_buff, SB_DEF_SIZE, PRIORITY_CMD, 0) >= 0){
        printf("once\n");
        switch(msg_buff.mtype){
            case INIT:
                
                rel = (struct SB_INT*)&msg_buff;
                //int receive_key = msg_buff.mdata[0];
                printf("Received key %d\n", rel->mint);
                clients[clients_no].connected = 1;
                clients[clients_no].friends_no = 0;
                clients[clients_no].key = rel->mint;
                clients[clients_no].msg = msgget(clients[clients_no].key, 0);
                if(clients[clients_no].msg == -1){
                    perror("Cannot open client queue");
                    exit(1);
                }
                rel->mtype = CONFIRM;
                rel->mint = clients_no;
                msgsnd(clients[clients_no].msg, rel, sizeof(rel->mint), 0);

                clients_no++;
                break;

            case ECHO:
                printf("RECEIVED ECHO from %d\n", msg_buff.sender);
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                msgsnd(clients[msg_buff.sender].msg, &msg_buff, SB_TEXT_SIZE, 0);
                break;

            case LIST:
                printf("Received LIST from %d\n", msg_buff.sender);
                strcpy(msg_buff.mtext, "");
                for(int i=0; i<clients_no; i++){
                    if(clients[i].connected == 1)
                        sprintf(msg_buff.mtext + strlen(msg_buff.mtext), "%d ", i);
                }
                msgsnd(clients[msg_buff.sender].msg, &msg_buff, SB_TEXT_SIZE, 0);
                break;

            case FRIENDS:
                printf("Received FRIENDS from %d\n", msg_buff.sender);
                struct SB_DEF *def_ptr = (struct SB_DEF*) &msg_buff;
                if(def_ptr->mdata[0] == -1){
                    //clear friends
                    clients[msg_buff.sender].friends_no = 0;
                    printf("cleared friends\n");
                }
                else{
                    int i=0;
                    for(; def_ptr->mdata[i] != -1; i++){
                        clients[msg_buff.sender].friends[i] = def_ptr->mdata[i];
                        printf("adding friend %d\n", def_ptr->mdata[i]);
                    }
                    clients[msg_buff.sender].friends_no = i;
                    printf("%d friends\n", i);
                }
                break;

            case TOALL:
                printf("RECEIVED 2ALL from %d\n", msg_buff.sender);
                msg_buff.mtype = ECHO;
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                strcat(msg_buff.mtext, "\tfrom client: ");
                
                sprintf(no, "%d", msg_buff.sender);
                strcat(msg_buff.mtext, no);
                for(int i=0; i<clients_no; i++){
                    if(clients[i].connected == 1){
                        msgsnd(clients[i].msg, &msg_buff, SB_TEXT_SIZE, 0);
                
                    }
                }
                break;

            case ADD:
                ;
                struct SB_DEF* sb_def = (struct SB_DEF*) &msg_buff;
                printf("RECEIVED ADD from %d\n", sb_def->sender);

                for( int i=0; sb_def->mdata[i] != -1; i++){
                    addFriend(&clients[sb_def->sender], sb_def->mdata[i]);
                }
                break;

            case DEL:
                ;
                struct SB_DEF* sb_deff = (struct SB_DEF*) &msg_buff;
                printf("RECEIVED DEL from %d\n", sb_deff->sender);

                for( int i=0; sb_deff->mdata[i] != -1; i++){
                    deleteFriend(&clients[sb_deff->sender], sb_deff->mdata[i]);
                }
                break;

            case TOFRIENDS:
                printf("RECEIVED 2FRIENDS from %d\n", msg_buff.sender);
                printf("cat: %s\n", msg_buff.mtext);
                msg_buff.mtype = ECHO;
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                //printf("Przed forem %s\n", ctime(&now));
                strcat(msg_buff.mtext, ctime(&now));
                strcat(msg_buff.mtext, "\tfrom client: ");
                
                sprintf(no, "%d", msg_buff.sender);
                strcat(msg_buff.mtext, no);
                for(int i=0; i<clients[msg_buff.sender].friends_no; i++){
                    printf("%d %d\n", clients[msg_buff.sender].friends[i], clients_no);
                    if(clients[msg_buff.sender].friends[i] >= clients_no){
                        printf("wrong friends index, ignoring...\n");
                        continue;
                    }
                    if(clients[clients[msg_buff.sender].friends[i]].connected == 1){
                        msgsnd(clients[clients[msg_buff.sender].friends[i]].msg, &msg_buff, SB_TEXT_SIZE, 0);
                
                    }else printf("client no longer connected, ignoring...\n");
                }
                break;

            case TOONE:
                printf("RECEIVED TOONE from %d\n", msg_buff.sender);
                strcat(msg_buff.mtext, "\n\t");
                msg_buff.mtype = ECHO;
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                if((msg_buff.receiver < clients_no) && (clients[msg_buff.receiver].connected == 1)){
                    msgsnd(clients[msg_buff.receiver].msg, &msg_buff, SB_TEXT_SIZE, 0);
                } else printf("client no longer connected, ignoring...\n");
                break;

            case CSTOP:
                ;
                struct SB_INT *sb_int = (struct SB_INT*)&msg_buff;
                printf("RECEIVED CSTOP from %d\n", sb_int->mint);
                clients[sb_int->mint].connected = 0;
                break;
        }
    }
    perror("error: ");
    printf("end\n");
    while(1) sleep(1);

    return 0;
}