#define INIT 11
#define CONFIRM 12
#define ECHO 13
#define LIST 2
#define FRIENDS 3
#define TOALL 16
#define TOFRIENDS 17
#define TOONE 18
#define CSTOP 1
#define ADD 4
#define DEL 5
#define PRIORITY_CMD -100

struct SB_TEXT {
    long mtype;
    int sender;
    int receiver;
    char mtext[100];
};

#define SB_TEXT_SIZE 2*sizeof(int)+100*sizeof(char)

struct SB_INT {
    long mtype;
    int mint;
};

struct SB_DEF{
    long mtype;
    int sender;
    int receiver;
    int mdata[100];
};
#define SB_DEF_SIZE 2*sizeof(int)+100*sizeof(int)