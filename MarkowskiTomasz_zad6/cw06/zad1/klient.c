#define _XOPEN_SOURCE 800
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/msg.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<signal.h>
#include "komunikaty.h"

int priv_queue;
int main_queue;
int my_index;
int child_pid;

void sigintHandler(int signo){
    if(child_pid != 0){
        msgctl(priv_queue, IPC_RMID, NULL);
        struct SB_INT sb_int;
        sb_int.mtype = CSTOP;
        sb_int.mint = my_index;
        msgsnd(main_queue, &sb_int, sizeof(sb_int.mint), 0);

        if(signo != 0) printf("\n");
        printf("Exiting...\n");
    }
    exit(0);
}

void sigusr1Handler(int signo){
        printf("Exiting...\n");
        msgctl(priv_queue, IPC_RMID, NULL);
        exit(0);
}


int main(int argc, char *argv[]){
    signal(SIGINT, sigintHandler);
    signal(SIGUSR1, sigusr1Handler);

    char *path = getenv("HOME");
    key_t main_key = ftok(path, 'a');
    main_queue = msgget(main_key, 0);
    if(main_queue == -1){
        perror("Cannot open server queue");
        exit(1);
    }

    int adder = 0;
    while((priv_queue = msgget(main_key+adder, IPC_CREAT|IPC_EXCL|0666)) == -1){
        printf("next\n");
        adder++;
    }
    key_t priv_key = main_key+adder;

    struct SB_INT msg_buff;
    msg_buff.mtype = INIT;
    msg_buff.mint = priv_key;
    //msg_buff.mtext = calloc(100, sizeof(char));
    //strcpy(msg_buff.mtext, "Wiadomosc");
    msgsnd(main_queue, &msg_buff, sizeof(msg_buff.mint), 0);

    msgrcv(priv_queue, &msg_buff, sizeof(msg_buff.mint), CONFIRM, 0);
    my_index = msg_buff.mint;
    printf("My index is: %d\n", my_index);

    if((child_pid = fork() )== 0){
        //child
        struct SB_TEXT msg_buff;
        //msg_buff.mtext = calloc(100, sizeof(char));
        //struct SB_INT *rel;
        while(msgrcv(priv_queue, &msg_buff, SB_TEXT_SIZE, PRIORITY_CMD, 0) >= 0){
            switch(msg_buff.mtype){
                case ECHO:
                    printf("Echo: %s\n", msg_buff.mtext);
                    break;
                case LIST:
                    printf("List: %s\n", msg_buff.mtext);
                    break;
                case CSTOP:
                    printf("Server shutdown...\n");
                    kill(getppid(), SIGUSR1);
                    exit(0);
                    break;
            }
        }
    }
    else{
        struct SB_TEXT msg_buff;

        FILE *input_file = stdin;
        //msg_buff.mtext = calloc(100, sizeof(char));
        //struct SB_INT *rel;

        char command[100];
        while(1){
            if(fscanf(input_file, "%s", command) <= 0){
                if(input_file != stdin) fclose(input_file);
                input_file = stdin;
            }

            if(strcmp(command, "echo") == 0){
                fscanf(input_file, "%s", command);
                msg_buff.sender = my_index;
                strcpy(msg_buff.mtext, command);
                msg_buff.mtype = ECHO;
                msgsnd(main_queue, &msg_buff,SB_TEXT_SIZE, 0);
            }
            else if(strcmp(command, "list") == 0){
                msg_buff.mtype = LIST;
                msg_buff.sender = my_index;
                msgsnd(main_queue, &msg_buff, SB_TEXT_SIZE, 0);
            }
            else if(strcmp(command, "friends") == 0){
                struct SB_DEF buff_def;
                buff_def.mtype = FRIENDS;
                buff_def.sender = my_index;
                buff_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; // usuniecie \n
                char *line_iter = line+1;

                line_iter = strtok(line_iter, " ");
                int i = 0;
                while(line_iter != NULL){
                    sscanf(line_iter,"%d",&(buff_def.mdata[i++]));
                    line_iter = strtok(NULL, " ");
                }
                buff_def.mdata[i] = -1; //guard
                msgsnd(main_queue, &buff_def, SB_DEF_SIZE, 0);
                printf("%d\n", i);
                free(line);
            }
            else if(strcmp(command, "2all") == 0){
                fscanf(input_file, "%s", command);
                msg_buff.sender = my_index;
                strcpy(msg_buff.mtext, command);
                msg_buff.mtype = TOALL;
                msgsnd(main_queue, &msg_buff,SB_TEXT_SIZE, 0);
            }
            else if(strcmp(command, "2friends") == 0){
                fscanf(input_file, "%s", command);
                msg_buff.sender = my_index;
                strcpy(msg_buff.mtext, command);
                msg_buff.mtype = TOFRIENDS;
                msgsnd(main_queue, &msg_buff,SB_TEXT_SIZE, 0);
            }
            else if(strcmp(command, "2one") == 0){
                int toWho;
                fscanf(input_file, "%d", &toWho);
                fscanf(input_file, "%s", command);
                msg_buff.sender = my_index;
                msg_buff.receiver = toWho;
                strcpy(msg_buff.mtext, command);
                msg_buff.mtype = TOONE;
                msgsnd(main_queue, &msg_buff,SB_TEXT_SIZE, 0);
            }
            else if(strcmp(command, "stop") == 0){
                kill(child_pid, SIGINT);
                sigintHandler(0);
            }
            else if(strcmp(command, "add") == 0){
                struct SB_DEF sb_def;
                sb_def.mtype = ADD;
                sb_def.sender = my_index;
                sb_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0;
                
                char *line_iter = line+1;
                line_iter = strtok(line_iter, " ");
                int count = 0;

                while(line_iter != NULL){
                    sscanf(line_iter, "%d", &(sb_def.mdata[count]));
                    count++;

                    line_iter = strtok(NULL, " ");
                }
                sb_def.mdata[count] = -1;
                
                if(count > 0)
                    msgsnd(main_queue, &sb_def, SB_DEF_SIZE, 0);
                else printf("Nothing to add, ignoring...\n");

                free(line);
            }
            else if(strcmp(command, "del") == 0){
                struct SB_DEF sb_def;
                sb_def.mtype = DEL;
                sb_def.sender = my_index;
                sb_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0;
                
                char *line_iter = line+1;
                line_iter = strtok(line_iter, " ");
                int count = 0;

                while(line_iter != NULL){
                    sscanf(line_iter, "%d", &(sb_def.mdata[count]));
                    count++;

                    line_iter = strtok(NULL, " ");
                }
                sb_def.mdata[count] = -1;
                
                if(count > 0)
                    msgsnd(main_queue, &sb_def, SB_DEF_SIZE, 0);
                else printf("Nothing to delete, ignoring...\n");

                free(line);
            }
            else if( (strcmp(command, "read") == 0) && (input_file == stdin) ){
                char *path = calloc(100, sizeof(char));
                scanf("%s", path);

                printf("reading from %s\n", path);
                FILE *opened_file = fopen(path, "r");
                if(opened_file == NULL){
                    perror("Cannot open file");
                    continue;
                    // kill(child_pid, SIGINT);
                    // exit(1);
                }

                input_file = opened_file;

                free(path);
            }
        }
    }

    while(1) sleep(1);
    return 0;
}