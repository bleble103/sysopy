#define INIT 1
#define CONFIRM 2
#define ECHO 3
#define LIST 14
#define FRIENDS 13
#define TOALL 6
#define TOFRIENDS 7
#define TOONE 8
#define CSTOP 15
#define ADD 12
#define DEL 11
#define PRIORITY_CMD -100

struct SB_TEXT {
    char mtype;
    char sender;
    char receiver;
    char mtext[100];
};

#define SB_TEXT_SIZE sizeof(struct SB_TEXT)

struct SB_INT {
    char mtype;
    int mint;
};

#define SB_INT_SIZE sizeof(struct SB_INT)

struct SB_DEF{
    char mtype;
    char sender;
    char receiver;
    int mdata[16];
};
#define SB_DEF_SIZE sizeof(struct SB_DEF)
