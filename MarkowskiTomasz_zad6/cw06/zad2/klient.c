#define _XOPEN_SOURCE 800
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<signal.h>
#include<fcntl.h>
#include<mqueue.h>
#include<time.h>
#include "komunikaty.h"

mqd_t priv_queue;
mqd_t main_queue;
int my_index;
int child_pid;
char *path;

void sigintHandler(int signo){
    if(child_pid != 0){
        struct SB_INT sb_int;
        sb_int.mtype = CSTOP;
        sb_int.mint = my_index;
        mq_send(main_queue, (char*)&sb_int, SB_INT_SIZE, CSTOP);

        if(signo != 0) printf("\n");
        printf("Exiting...\n");

        sleep(1);
        if(mq_unlink(path) == -1){
            perror("Cannot unlink private queue");
            exit(1);
        }
    }
    exit(0);
}

void sigusr1Handler(int signo){
        printf("Exiting...\n");
        if(mq_unlink(path) == -1){
            perror("Cannot unlink private queue");
            exit(1);
        }
        exit(0);
}


int main(int argc, char *argv[]){
    srand(time(NULL));

    signal(SIGINT, sigintHandler);
    signal(SIGUSR1, sigusr1Handler);

    main_queue = mq_open("/bleserv", O_WRONLY);
    if(main_queue == -1){
        perror("Cannot open server queue");
        exit(1);
    }

    path = calloc(10, sizeof(char));
    int randomPart = rand()%10000;
    int adder = 0;
    sprintf(path, "/cli%d", randomPart+adder);

    struct mq_attr attr;  
    attr.mq_flags = 0;  
    attr.mq_maxmsg = 10;  
    attr.mq_msgsize = SB_TEXT_SIZE;  
    attr.mq_curmsgs = 0;  

    while((priv_queue = mq_open(path, O_RDONLY|O_CREAT, 0666, &attr)) == -1){
        printf("next\n");
        adder++;

        sprintf(path, "/cli%d", randomPart+adder);
    }

    struct SB_TEXT msg_buff;
    msg_buff.mtype = INIT;
    strcpy(msg_buff.mtext, path);
    if(mq_send(main_queue, (char*)&msg_buff, SB_TEXT_SIZE, INIT) == -1){
        perror("Cannot send init to server");
        exit(1);
    }

    struct SB_INT sb_intb;
    unsigned int prio;
    if(mq_receive(priv_queue, (char*)&sb_intb, SB_TEXT_SIZE, &prio) == -1){
        perror("Cannot receive init");
        exit(1);
    }
    my_index = sb_intb.mint;
    printf("My index is: %d\n", my_index);

    if((child_pid = fork() )== 0){
        //child
        struct SB_TEXT msg_buff;
        while(mq_receive(priv_queue, (char*)&msg_buff, SB_TEXT_SIZE, &prio) >= 0){
            switch(prio){
                case ECHO:
                    printf("Echo: %s\n", msg_buff.mtext);
                    break;
                case LIST:
                    printf("List: %s\n", msg_buff.mtext);
                    break;
                case CSTOP:
                    printf("Server shutdown...\n");
                    kill(getppid(), SIGUSR1);
                    exit(0);
                    break;
            }
        }
    }
    else{
        struct SB_TEXT msg_buff;

        FILE *input_file = stdin;

        char command[100];
        while(1){
            if(fscanf(input_file, "%s", command) <= 0){
                if(input_file != stdin) fclose(input_file);
                input_file = stdin;
            }

            if(strcmp(command, "echo") == 0){
                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; //remove newline

                msg_buff.sender = my_index;
                strcpy(msg_buff.mtext, line+1); //ignore first space
                msg_buff.mtype = ECHO;
                mq_send(main_queue, (char*)&msg_buff,SB_TEXT_SIZE, ECHO);

                free(line);
            }
            else if(strcmp(command, "list") == 0){
                msg_buff.mtype = LIST;
                msg_buff.sender = my_index;
                mq_send(main_queue, (char*)&msg_buff, SB_TEXT_SIZE, LIST);
            }
            else if(strcmp(command, "friends") == 0){
                struct SB_DEF buff_def;
                buff_def.mtype = FRIENDS;
                buff_def.sender = my_index;
                buff_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; // usuniecie \n
                char *line_iter = line+1;

                line_iter = strtok(line_iter, " ");
                int i = 0;
                while(line_iter != NULL){
                    sscanf(line_iter,"%d",&(buff_def.mdata[i++]));
                    line_iter = strtok(NULL, " ");
                }
                buff_def.mdata[i] = -1; //guard
                mq_send(main_queue, (char*)&buff_def, SB_DEF_SIZE, FRIENDS);
                printf("%d\n", i);
                free(line);
            }
            else if(strcmp(command, "2all") == 0){
                msg_buff.sender = my_index;
                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; //remove newline
                strcpy(msg_buff.mtext, line+1);
                free(line);

                msg_buff.mtype = TOALL;
                mq_send(main_queue, (char*)&msg_buff,SB_TEXT_SIZE, TOALL);
            }
            else if(strcmp(command, "2friends") == 0){
                msg_buff.sender = my_index;
                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; //remove newline
                strcpy(msg_buff.mtext, line+1);
                free(line);

                msg_buff.mtype = TOFRIENDS;
                mq_send(main_queue, (char*)&msg_buff,SB_TEXT_SIZE, TOFRIENDS);
            }
            else if(strcmp(command, "2one") == 0){
                int toWho;
                fscanf(input_file, "%d", &toWho);

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0; //remove newline
                strcpy(msg_buff.mtext, line+1);
                free(line);

                msg_buff.sender = my_index;
                msg_buff.receiver = toWho;
                msg_buff.mtype = TOONE;
                mq_send(main_queue, (char*)&msg_buff,SB_TEXT_SIZE, TOONE);
            }
            else if(strcmp(command, "stop") == 0){
                kill(child_pid, SIGINT);
                sigintHandler(0);
            }
            else if(strcmp(command, "add") == 0){
                struct SB_DEF sb_def;
                sb_def.mtype = ADD;
                sb_def.sender = my_index;
                sb_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0;
                
                char *line_iter = line+1;
                line_iter = strtok(line_iter, " ");
                int count = 0;

                while(line_iter != NULL){
                    sscanf(line_iter, "%d", &(sb_def.mdata[count]));
                    count++;

                    line_iter = strtok(NULL, " ");
                }
                sb_def.mdata[count] = -1;
                
                if(count > 0)
                    mq_send(main_queue, (char*)&sb_def, SB_DEF_SIZE, ADD);
                else printf("Nothing to add, ignoring...\n");

                free(line);
            }
            else if(strcmp(command, "del") == 0){
                struct SB_DEF sb_def;
                sb_def.mtype = DEL;
                sb_def.sender = my_index;
                sb_def.mdata[0] = -1;

                char *line = calloc(100, sizeof(char));
                size_t len = 100;
                getline(&line, &len, input_file);
                line[strlen(line)-1] = 0;
                
                char *line_iter = line+1;
                line_iter = strtok(line_iter, " ");
                int count = 0;

                while(line_iter != NULL){
                    sscanf(line_iter, "%d", &(sb_def.mdata[count]));
                    count++;

                    line_iter = strtok(NULL, " ");
                }
                sb_def.mdata[count] = -1;
                
                if(count > 0)
                    mq_send(main_queue, (char*)&sb_def, SB_DEF_SIZE, DEL);
                else printf("Nothing to delete, ignoring...\n");

                free(line);
            }
            else if( (strcmp(command, "read") == 0) && (input_file == stdin) ){
                char *path = calloc(100, sizeof(char));
                scanf("%s", path);

                printf("reading from %s\n", path);
                FILE *opened_file = fopen(path, "r");
                if(opened_file == NULL){
                    perror("Cannot open file");
                    continue;
                }

                input_file = opened_file;

                free(path);
            }
        }
    }

    while(1) sleep(1);
    return 0;
}