#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/msg.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<signal.h>
#include<time.h>
#include<fcntl.h>
#include<mqueue.h>
#include "komunikaty.h"

struct client{
    mqd_t msg;
    int friends[15];
    int friends_no;
    int connected;
};

struct client clients[100];
int clients_no = 0;

void addFriend(struct client *client, int friend){
    for(int i=0; i<client->friends_no; i++){
        if(client->friends[i] == friend) return; //already a friend
    }
    client->friends[client->friends_no++] = friend;
}

void deleteFriend(struct client *client, int friend){
    for(int i=0; i<client->friends_no; i++){
        if(client->friends[i] == friend){
            for(int j=i; j<client->friends_no-1; j++){
                client->friends[j] = client->friends[j+1];
            }
            client->friends_no--;

            return;
        }
    }
}

mqd_t main_queue;

void sigintHandler(int signo){
    msgctl(main_queue, IPC_RMID, NULL);

    /* informowanie klientów */
    struct SB_INT sb_int;
    sb_int.mtype = CSTOP;

    for(int i=0; i<clients_no; i++){
        if(clients[i].connected == 1){
            mq_send(clients[i].msg, (char*)&sb_int, sizeof(int), CSTOP);
        }
    }
    printf("Closing queue...\n");
    sleep(1);

    if(mq_close(main_queue) == -1){
        perror("Cannot close queue");
        exit(1);
    }

    if(mq_unlink("/bleserv") == -1){
        perror("Cannot unlink queue");
        exit(1);
    }

    exit(0);
}

int main(int argc, char *argv[]){
    struct mq_attr attr;  
    attr.mq_flags = 0;  
    attr.mq_maxmsg = 10;  
    attr.mq_msgsize = SB_TEXT_SIZE;  
    attr.mq_curmsgs = 0;  

    main_queue = mq_open("/bleserv", O_RDONLY|O_CREAT, 0666, &attr);
    if(main_queue == -1){
        perror("Cannot create server queue");
        exit(1);
    }



    signal(SIGINT, sigintHandler);


    struct SB_TEXT msg_buff;
    time_t now;
    static char no[5];
    unsigned int prio;
    while(mq_receive(main_queue, (char*)&msg_buff, SB_TEXT_SIZE, &prio) >= 0){
        printf("once\n");
        switch(prio){
            case INIT:
                
                printf("Received path %s\n", msg_buff.mtext);
                clients[clients_no].connected = 1;
                clients[clients_no].friends_no = 0;
                clients[clients_no].msg = mq_open(msg_buff.mtext, O_WRONLY);
                if(clients[clients_no].msg == -1){
                    perror("Cannot open client queue");
                    exit(1);
                }
                struct SB_INT int_confirm;
                int_confirm.mtype = CONFIRM;
                int_confirm.mint = clients_no;
                printf("Sending confirm to %d\n", int_confirm.mint);
                mq_send(clients[clients_no].msg, (char*)&int_confirm, SB_INT_SIZE, CONFIRM);

                clients_no++;
                break;

            case ECHO:
                printf("RECEIVED ECHO from %d\n", msg_buff.sender);
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                mq_send(clients[(int)msg_buff.sender].msg, (char*)&msg_buff, SB_TEXT_SIZE, ECHO);
                break;

            case LIST:
                printf("Received LIST from %d\n", msg_buff.sender);
                strcpy(msg_buff.mtext, "");
                for(int i=0; i<clients_no; i++){
                    if(clients[i].connected == 1)
                        sprintf(msg_buff.mtext + strlen(msg_buff.mtext), "%d ", i);
                }
                mq_send(clients[(int)msg_buff.sender].msg, (char*)&msg_buff, SB_TEXT_SIZE, LIST);
                break;

            case FRIENDS:
                printf("Received FRIENDS from %d\n", msg_buff.sender);
                struct SB_DEF *def_ptr = (struct SB_DEF*) &msg_buff;
                if(def_ptr->mdata[0] == -1){
                    //clear friends
                    clients[(int)msg_buff.sender].friends_no = 0;
                    printf("cleared friends\n");
                }
                else{
                    int i=0;
                    for(; def_ptr->mdata[i] != -1; i++){
                        clients[(int)msg_buff.sender].friends[i] = def_ptr->mdata[i];
                        printf("adding friend %d\n", def_ptr->mdata[i]);
                    }
                    clients[(int)msg_buff.sender].friends_no = i;
                    printf("%d friends\n", i);
                }
                break;

            case TOALL:
                printf("RECEIVED 2ALL from %d\n", msg_buff.sender);
                msg_buff.mtype = ECHO;
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                strcat(msg_buff.mtext, "\tfrom client: ");
                
                sprintf(no, "%d", msg_buff.sender);
                strcat(msg_buff.mtext, no);
                for(int i=0; i<clients_no; i++){
                    if(clients[i].connected == 1){
                        mq_send(clients[i].msg, (char*)&msg_buff, SB_TEXT_SIZE, ECHO);
                
                    }
                }
                break;

            case ADD:
                ;
                struct SB_DEF* sb_def = (struct SB_DEF*) &msg_buff;
                printf("RECEIVED ADD from %d\n", sb_def->sender);

                for( int i=0; sb_def->mdata[i] != -1; i++){
                    addFriend(&clients[(int)sb_def->sender], sb_def->mdata[i]);
                }
                break;

            case DEL:
                ;
                struct SB_DEF* sb_deff = (struct SB_DEF*) &msg_buff;
                printf("RECEIVED DEL from %d\n", sb_deff->sender);

                for( int i=0; sb_deff->mdata[i] != -1; i++){
                    deleteFriend(&clients[(int)sb_deff->sender], sb_deff->mdata[i]);
                }
                break;

            case TOFRIENDS:
                printf("RECEIVED 2FRIENDS from %d\n", msg_buff.sender);
                printf("cat: %s\n", msg_buff.mtext);
                msg_buff.mtype = ECHO;
                strcat(msg_buff.mtext, "\n\t");
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                strcat(msg_buff.mtext, "\tfrom client: ");
                
                sprintf(no, "%d", msg_buff.sender);
                strcat(msg_buff.mtext, no);
                for(int i=0; i<clients[(int)msg_buff.sender].friends_no; i++){
                    printf("%d %d\n", clients[(int)msg_buff.sender].friends[i], clients_no);
                    if(clients[(int)msg_buff.sender].friends[i] >= clients_no){
                        printf("wrong friends index, ignoring...\n");
                        continue;
                    }
                    if(clients[clients[(int)msg_buff.sender].friends[i]].connected == 1){
                        mq_send(clients[clients[(int)msg_buff.sender].friends[i]].msg, (char*)&msg_buff, SB_TEXT_SIZE, ECHO);
                
                    }else printf("client no longer connected, ignoring...\n");
                }
                break;

            case TOONE:
                printf("RECEIVED TOONE from %d\n", msg_buff.sender);
                strcat(msg_buff.mtext, "\n\t");
                msg_buff.mtype = ECHO;
                now = time(0);
                strcat(msg_buff.mtext, ctime(&now));
                if((msg_buff.receiver < clients_no) && (clients[(int)msg_buff.receiver].connected == 1)){
                    mq_send(clients[(int)msg_buff.receiver].msg, (char*)&msg_buff, SB_TEXT_SIZE, ECHO);
                } else printf("client no longer connected, ignoring...\n");
                break;

            case CSTOP:
                ;
                struct SB_INT *sb_int = (struct SB_INT*)&msg_buff;
                printf("RECEIVED CSTOP from %d\n", sb_int->mint);
                mq_close(clients[(int)sb_int->mint].msg);
                clients[(int)sb_int->mint].connected = 0;
                break;
        }
    }
    perror("error: ");
    printf("end\n");
    while(1) sleep(1);

    return 0;
}