#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>

int working = 1;
pid_t fpid = 0;

void proceduraSIGSTP(){
    if(working == 1){
        printf("\nOczekuje na CTRL+Z - kontynuacja albo CTR+C - zakonczenie programu\n");
        kill(fpid, 9);
        working = 0;
    }
    else if(working == 0){
        working = 1;
        fpid = fork();
        if(fpid == 0){
            execlp("./skrypt.sh","./skrypt.sh",NULL);
        }
    }
}

void proceduraSIGINT(){
    printf("\nOdebrano sygnał SIGINT\n");
    if(working == 1){
        kill(fpid, SIGINT);
    }
    exit(0);
}

int main(int argc, char *argv[]){
    fpid = fork();
    if(fpid == 0){
        execlp("./skrypt.sh","./skrypt.sh",NULL);
    }
    signal(20, proceduraSIGSTP);

    /* sigaction */
    struct sigaction sga;
    sga.sa_handler = proceduraSIGINT;
    sigemptyset(&sga.sa_mask);
    sga.sa_flags = 0;
    sigaction(SIGINT, &sga, NULL);

    while(1){

    }
    return 0;
}
