#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>
#include<unistd.h>
#include<string.h>

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

void randomize(char *buff, size_t length, time_t s_time){
    char *rand_str = calloc(length+1, sizeof(char)); //+ nullbyte
    for(int i=0; i<length; i++){
        rand_str[i] = rand()%26 + 97; //literki
    }
    time_t now = time(NULL);
    char *time_str = calloc(25, sizeof(char));
    strftime(time_str, 25, "%Y-%m-%d_%H-%M-%S", localtime(&now));
    sprintf(buff, "pid: %d, s_time: %ld, %s %s\n", getpid(), s_time, time_str, rand_str);
    free(rand_str);
    free(time_str);

}

int main(int argc, char *argv[]){
    if(argc != 5) error("wrong arguments");
    if(isNum(argv[2]) * isNum(argv[3]) * isNum(argv[4]) == 0) error("wrong arguments");
    int pmin = atoi(argv[2]);
    int pmax = atoi(argv[3]);
    size_t bytes = atoi(argv[4]);
    if(!(pmax > pmin) || bytes < 1) error("wrong arguments");

    srand(time(NULL));
    char *random_string = calloc(bytes+50, sizeof(char)); //+ reszta informacji we wpisie
    while(1){
        time_t s_time = rand()%(pmax-pmin+1) + pmin;
        randomize(random_string, bytes, s_time);
        //printf("sleeping %ld\n", s_time);
        //printf("random string %s\n", random_string);
        FILE *fp = fopen(argv[1], "a"); //append
        if(fp == NULL) error("cannot open file");
        size_t entry_length = strlen(random_string);
        if(fwrite(random_string, 1, entry_length, fp) != entry_length) error("cannot write to file");
        
        fclose(fp);
        printf("file changed!\n");
        sleep(s_time);
    }
    
    return 0;
}