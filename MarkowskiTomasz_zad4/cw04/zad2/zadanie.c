#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<string.h>
#include<ctype.h>

#define COPY_MEM 0
#define COPY_CP 1

int copy_type;
int whole_time = 0;

void get_filename(char *path, char *buff, time_t mtime){
    int len = strlen(path);
    path += len;
    while(*path != '/') path--;
    path++;
    strcpy(buff, "archiwum/");
    strcat(buff, path);
    char *time_str = calloc(25, sizeof(char));
    strftime(time_str, 25, "_%Y-%m-%d_%H-%M-%S", localtime(&mtime));
    strcat(buff, time_str);
    free(time_str);
}

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}
void trim_slash(char *path){
    //usun ewentualny '/' na końcu ścieżki
    while(*path != 0) path++;
    path--;
    if(*path == '/') *path = 0;
}

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int controllFlag = 1;
int changes = 0;

/* sygnał stopowania */
void sigusr1Handler(){
    printf("Paused %d\n", getpid());
    controllFlag = 0;
}

/* sygnał wznawiania */
void sigusr2Handler(){
    printf("Resumed %d\n", getpid());
    controllFlag = 1;
}

/* sigint */
void sigintHandler(){
    exit(changes);
}

void monitor(char *path, int seconds){
    /* Handlery sygnałów */
    signal(SIGUSR1, sigusr1Handler);
    signal(SIGUSR2, sigusr2Handler);
    signal(SIGINT, sigintHandler);
    
    char *file_content = NULL;
    int length = 0;

    changes = 0;    //liczba zmian w danym procesie
    char *absolute_path = calloc(200, sizeof(char));
    if(path[0] == '/'){
        trim_slash(path);
        absolute_path = strcpy(path, absolute_path);
    }
    else{
        char *realpath_result = realpath(path, absolute_path);
        if(realpath_result == NULL){
            printf("error: %s\n", path);
            error("cannot monitor file");
        }
    }
    //sciezka w absolute_path


    if(copy_type == COPY_MEM){
        //tresc pliku do pamieci
        FILE *fp = fopen(absolute_path, "r");
        fseek(fp, 0L, SEEK_END);
        length = ftell(fp);
        fseek(fp, 0L, SEEK_SET); //powrot
        file_content = calloc(length, sizeof(char));
        if(fread(file_content, 1, length, fp) != length) error("cannot read file");
        fclose(fp);
    }
    time_t mtime;
    struct stat stat_buff;
    stat(absolute_path, &stat_buff);
    mtime = stat_buff.st_mtime;
    while(1){
        if(!controllFlag) continue;
        sleep(seconds);
        //printf("Monitor: %d\n", getpid());
        whole_time -= seconds;

        stat(absolute_path, &stat_buff);
        if(stat_buff.st_mtime != mtime){
            printf("CHANGED!!!\n");
            changes++;
            mtime = stat_buff.st_mtime;
            if(copy_type == COPY_MEM){
                //utworzenie kopii z czasem
                char *filename = calloc(50, sizeof(char));
                get_filename(absolute_path, filename, mtime);
                FILE *fp = fopen(filename, "w");
                //printf("tworzenie: %s\n", filename);
                if(fwrite(file_content, 1, length, fp) != length) error("cannot write to file");
                fclose(fp);
                free(file_content);
                free(filename);
                
                fp = fopen(absolute_path, "r");
                fseek(fp, 0L, SEEK_END);
                length = ftell(fp);
                fseek(fp, 0L, SEEK_SET);
                file_content = calloc(length, sizeof(char));
                if(fread(file_content, 1, length, fp) != length) error("cannot read file");
                fclose(fp);
            }
            else if(copy_type == COPY_CP){
                //utworzenie kopii z czasem przy pomocy fork -> execlp
                if(vfork() == 0){
                    //potomek
                    char *filename = calloc(50, sizeof(char));
                    get_filename(absolute_path, filename, mtime);
                    execlp("cp", "cp", absolute_path, filename, NULL);
                    free(filename);
                }
            }
        }
    }
    free(absolute_path);
    exit(changes);
}

struct child {
    pid_t pid;
    int working;
};

int findChild(struct child *children, int children_no, int pid){
    for(int i=0; i<children_no; i++){
        if(children[i].pid == pid) return i;
    }
    return -1;
}

int main(int argc, char *argv[]){
    struct child *children = calloc(100, sizeof(struct child));
    int children_no = 0;
    whole_time = 10;

    if(argc < 3) error("wrong arguments");
    FILE *lista = fopen(argv[1], "r");
    if(lista == NULL) error("cannot open file");

    if(strcmp(argv[2], "mem") == 0) copy_type = COPY_MEM;
    else if(strcmp(argv[2], "cp") == 0) copy_type = COPY_CP;
    else error("wrong arguments");
		
    char *path_buff = calloc(200, sizeof(char));
    int seconds;
    while(fscanf(lista, "%s %d\n", path_buff, &seconds) > 0){
        pid_t pid = fork();
        if(pid == 0){
            monitor(path_buff, seconds);
        }
        else{
            children[children_no].pid = pid;
            children[children_no].working = 1;
            ++children_no;
        }
        //printf("plik: %s, co %d sekund \n", path_buff, seconds);
    }

    char commandBuff[50];
    while(1){
        scanf("%s", commandBuff);
        if(strcmp(commandBuff, "LIST") == 0){
            printf("Monitors:\n");
            for(int i=0; i<children_no; i++){
                printf("\t%d", children[i].pid);
                if(children[i].working) printf("\tWORKING\n");
                else printf("\tSTOPPED\n");
            }
        }
        else if(strcmp(commandBuff, "STOP") == 0){
            int arg_pid;
            if(scanf("%d", &arg_pid) < 1){
                scanf("%s", commandBuff);
                if(strcmp(commandBuff, "ALL") == 0){
                    /* Kill all */
                    for(int i=0; i<children_no; i++){
                        if(children[i].working){
                            kill(children[i].pid, SIGUSR1);
                            children[i].working = 0;
                        }
                    }

                }else{
                    printf("wrong command\n");
                }
            }
            else{
                int which_child = findChild(children, children_no, arg_pid);
                if(which_child >= 0){
                    kill(arg_pid, SIGUSR1);
                    children[which_child].working = 0;
                }else{
                    printf("wrong PID\n");
                }

            }
        }
        else if(strcmp(commandBuff, "START") == 0){
            int arg_pid;
            if(scanf("%d", &arg_pid) < 1){
                scanf("%s", commandBuff);
                if(strcmp(commandBuff, "ALL") == 0){
                    /* start all */
                    for(int i=0; i<children_no; i++){
                        if(children[i].working == 0){
                            kill(children[i].pid, SIGUSR2);
                            children[i].working = 1;
                        }
                    }

                }else{
                    printf("wrong command\n");
                }
            }
            else{
                int which_child = findChild(children, children_no, arg_pid);
                if(which_child >= 0){
                    kill(arg_pid, SIGUSR2);
                    children[which_child].working = 1;
                }else{
                    printf("wrong PID\n");
                }

            }
        }
        else if(strcmp(commandBuff, "END") == 0){
            for(int i=0; i<children_no; i++){
                kill(children[i].pid, SIGINT);
            }
            while(children_no > 0){
                int status;
                pid_t which = wait(&status);
                status >>= 8;
                printf("PID %d: %d changes\n", which, status);
                children_no--;
            }
            return 0;
        }
        else{
            printf("Wrong command\n");
        }
    }

    /*
    while(children_no > 0){
        int status;
        pid_t which = wait(&status);
        status >>= 8;
        printf("PID %d: %d changes\n", which, status);
        children_no--;
    }
    */

    free(path_buff);
    fclose(lista);
    return 0;
}
