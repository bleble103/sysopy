#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<sys/resource.h>
#include<string.h>
#include<ctype.h>

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}


int sender_pid = 0;
int received_signals = 0;

void sigusr1_handler(int signo, siginfo_t *siginfo, void *ucontext){
    sender_pid = siginfo->si_pid;
    received_signals++;

    //poinformuj sender że odebrane
    union sigval svalue;
    svalue.sival_int = 1;
    sigqueue(sender_pid, SIGUSR1, svalue);
}

void sigusr2_handler(int signo, siginfo_t *siginfo, void *ucontext){

    //odpowiedz senderowi że koniec
    union sigval svalue;
    svalue.sival_int = received_signals;
    sigqueue(sender_pid, SIGUSR2, svalue);
    printf("%d signals received\n", received_signals);
    exit(0);
}

int main(int argc, char *argv[]){
    /* Ignore all signals */
    for(int i=1; i<= 63; i++){
        signal(i, SIG_IGN);
    }
    

    /* Set SIGUSR1 action */
    struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr1_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    sigaction(SIGUSR1, &act, NULL); //ustaw
    

    /* Set SIGUSR2 action */
    //struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr2_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    sigaction(SIGUSR2, &act, NULL); //ustaw

    printf("Catcher's PID: %d\n", getpid());

    while(1){};

    return 0;
}
