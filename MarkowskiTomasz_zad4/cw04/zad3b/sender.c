#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<sys/resource.h>
#include<string.h>
#include<ctype.h>


void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}


int catcher_pid = 0;
int signals_no = 0;
int received_signals = 0;
int can_send = 1;   //blokada wysyłania do czasu potwierdzenia odbioru przez catcher

//int ending = 0;

void sigusr1_handler(int signo, siginfo_t *siginfo, void *ucontext){
    // (siginfo->si_value).sival_int
    can_send = 1; // odblokuj wysyłanie
    received_signals++;
}

void sigusr2_handler(int signo, siginfo_t *siginfo, void *ucontext){
    printf("Catcher received %d signals out of %d sent\n", (siginfo->si_value).sival_int, signals_no);
    printf("%d confirmations from catcher received\n", received_signals);
    exit(0);
}

int main(int argc, char *argv[]){
    /* Ignore all signals */
    //for(int i=1; i<= 63; i++){
    //    signal(i, SIG_IGN);
    //}

    if(argc < 3) error("wrong arguments");

    if(isNum(argv[1]) * isNum(argv[2]) == 0) error("wrong arguments");
    catcher_pid = atoi(argv[1]);
    signals_no = atoi(argv[2]);

    /* Set SIGUSR1 action */
    struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr1_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    sigaction(SIGUSR1, &act, NULL); //ustaw

    /* Set SIGUSR2 action */
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr2_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    sigaction(SIGUSR2, &act, NULL); //ustaw

    /* Wysyłanie sygnałów */
    union sigval svalue;
    for(int i=0; i<signals_no; i++){
        while(can_send == 0) {}; //wait with sending
        can_send = 0; //block next send
        svalue.sival_int = i+1;
        sigqueue(catcher_pid, SIGUSR1, svalue);
    }
    while(can_send == 0) {}; //wait with sending
    sigqueue(catcher_pid, SIGUSR2, svalue);
    


    while(1){};

    return 0;
}
