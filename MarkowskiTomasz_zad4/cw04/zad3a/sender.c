#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<sys/resource.h>
#include<string.h>
#include<ctype.h>

#define MODE_KILL 0
#define MODE_SIGQUEUE 1
#define MODE_SIGRT 2

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}

int mode;

int catcher_pid = 0;
int signals_no = 0;
int received_signals = 0;
int last_received_id = 0;
//int ending = 0;

void sigusr1_handler(int signo, siginfo_t *siginfo, void *ucontext){

    /* ustaw ostatni odebrany identyfikator */
    if(mode == MODE_SIGQUEUE){
        last_received_id = (siginfo->si_value).sival_int;
    }

    received_signals++;
}

void sigusr2_handler(int signo, siginfo_t *siginfo, void *ucontext){
    printf("Received back %d signals out of %d sent\n", received_signals, signals_no);

    /* Wypisz podsumowanie o siginfo value */
    if(mode == MODE_SIGQUEUE){
        printf("Last received index: %d, catcher received: %d\n", last_received_id, (siginfo->si_value).sival_int);
    }

    exit(0);
}

int main(int argc, char *argv[]){
    /* Ignore all signals */
    //for(int i=1; i<= 63; i++){
    //    signal(i, SIG_IGN);
    //}

    if(argc < 4) error("wrong arguments");
    if(strcmp(argv[3], "KILL") == 0){
        mode = MODE_KILL;
    }
    else if(strcmp(argv[3], "SIGQUEUE") == 0){
        mode = MODE_SIGQUEUE;
    }
    else if(strcmp(argv[3], "SIGRT") == 0){
        mode = MODE_SIGRT;
    }else error("wrong argument");
    if(isNum(argv[1]) * isNum(argv[2]) == 0) error("wrong arguments");
    catcher_pid = atoi(argv[1]);
    signals_no = atoi(argv[2]);

    /* Set SIGUSR1 action */
    struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr1_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    if(mode == MODE_KILL || mode == MODE_SIGQUEUE){
        sigaction(SIGUSR1, &act, NULL); //ustaw
    }
    else if(mode == MODE_SIGRT){
        sigaction(SIGRTMIN+1, &act, NULL); //ustaw
    }

    /* Set SIGUSR2 action */
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr2_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    if(mode == MODE_KILL || mode == MODE_SIGQUEUE){
        sigaction(SIGUSR2, &act, NULL); //ustaw
    }
    else if(mode == MODE_SIGRT){
        sigaction(SIGRTMIN+2, &act, NULL); //ustaw
    }

    /* Wysyłanie sygnałów w zalezności od trybu */
    if(mode == MODE_KILL){
        for(int i=0; i<signals_no; i++){
            kill(catcher_pid, SIGUSR1);
        }

        kill(catcher_pid, SIGUSR2);
    }
    else if(mode == MODE_SIGRT){
        for(int i=0; i<signals_no; i++){
            kill(catcher_pid, SIGRTMIN+1);
        }

        kill(catcher_pid, SIGRTMIN+2);
    }
    else if(mode == MODE_SIGQUEUE){
        union sigval svalue;
        for(int i=0; i<signals_no; i++){
            svalue.sival_int = i+1;
            sigqueue(catcher_pid, SIGUSR1, svalue);
        }
        sigqueue(catcher_pid, SIGUSR2, svalue);
    }
    


    while(1){};

    return 0;
}
