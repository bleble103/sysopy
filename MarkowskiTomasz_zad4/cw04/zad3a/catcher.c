#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<sys/resource.h>
#include<string.h>
#include<ctype.h>

#define MODE_KILL 0
#define MODE_SIGQUEUE 1
#define MODE_SIGRT 2

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int mode;

//int send_back = 0;
int sender_pid = 0;
int received_signals = 0;

void sigusr1_handler(int signo, siginfo_t *siginfo, void *ucontext){
    sender_pid = siginfo->si_pid;
    //printf("Sending pid: %d\n", sender_pid);
    received_signals++;
}

void sigusr2_handler(int signo, siginfo_t *siginfo, void *ucontext){
    sender_pid = siginfo->si_pid;
    //printf("Received SIGUSR2 from pid: %d\n", sender_pid);
    if(mode == MODE_KILL){
        for(int i=0; i<received_signals; i++){
            kill(sender_pid, SIGUSR1);
        }
        kill(sender_pid, SIGUSR2);
    }
    else if(mode == MODE_SIGRT){
        for(int i=0; i<received_signals; i++){
            kill(sender_pid, SIGRTMIN+1);
        }
        kill(sender_pid, SIGRTMIN+2);
    }
    else if(mode == MODE_SIGQUEUE){
        union sigval svalue;
        for(int i=0; i<received_signals; i++){
            svalue.sival_int = i+1;
            sigqueue(sender_pid, SIGUSR1, svalue);
        }
        sigqueue(sender_pid, SIGUSR2, svalue);
    }
    printf("%d signals received\n", received_signals);
    exit(0);
}

int main(int argc, char *argv[]){
    /* Ignore all signals */
    for(int i=1; i<= 63; i++){
        signal(i, SIG_IGN);
    }
    if(argc < 2) error("wrong arguments");
    if(strcmp(argv[1], "KILL") == 0){
        mode = MODE_KILL;
    }
    else if(strcmp(argv[1], "SIGQUEUE") == 0){
        mode = MODE_SIGQUEUE;
    }
    else if(strcmp(argv[1], "SIGRT") == 0){
        mode = MODE_SIGRT;
    }else error("wrong argument");

    /* Set SIGUSR1 action */
    struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr1_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    if(mode == MODE_KILL || mode == MODE_SIGQUEUE){
        sigaction(SIGUSR1, &act, NULL); //ustaw
    }
    else if(mode == MODE_SIGRT){
        sigaction(SIGRTMIN+1, &act, NULL); //ustaw
    }

    /* Set SIGUSR2 action */
    //struct sigaction act;
    act.sa_flags = SA_SIGINFO; //uzywaj sa_sigaction
    act.sa_sigaction = sigusr2_handler;
    sigemptyset(&act.sa_mask); //nicnie blokuj
    if(mode == MODE_KILL || mode == MODE_SIGQUEUE){
        sigaction(SIGUSR2, &act, NULL); //ustaw
    }
    else if(mode == MODE_SIGRT){
        sigaction(SIGRTMIN+2, &act, NULL); //ustaw
    }

    printf("Catcher's PID: %d\n", getpid());

    while(1){};

    return 0;
}
