
#define _XOPEN_SOURCE 500
#define _DEFAULT_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include<string.h>
#include<sys/stat.h>

#include<ftw.h>

#include<time.h>
#define S_EARLIER -1
#define S_SAME 0
#define S_LATER 1

void trim_slash(char *path){
    //usun ewentualny '/' na końcu ścieżki
    while(*path != 0) path++;
    path--;
    if(*path == '/') *path = 0;
}

void error(char* msg){
    fprintf(stderr,"%s\n", msg);
    exit(1);
}

time_t timestamp;
int when = 0;

int trav_fun(const char* fpath, const struct stat *stat_buff, int tflag, struct FTW *ftw_buff){
    //nie wypisuj katalogu poczatkowego
    if(ftw_buff->level == 0) return 0;

    //kryterium czasu
    int time_ok = 1;
    switch(when){
        case S_EARLIER:
            if(stat_buff->st_mtime >= timestamp) time_ok = 0;
            break;
        case S_SAME:
            //cala minuta
            if(stat_buff->st_mtime < timestamp || stat_buff->st_mtime > timestamp+59) time_ok = 0;
            break;
        case S_LATER:
            if(!(stat_buff->st_mtime > timestamp+59)) time_ok = 0;
            break;
    }
    //wypisz jesli spelnia kryterium czasu
    if(time_ok == 1){
        printf("\npath: %s\n", fpath);

        printf("\t%-20s %ld\n","size:", stat_buff->st_size);
        printf("\t%-20s ", "type:");
        if(S_ISLNK(stat_buff->st_mode))
            printf("slink\n");
        else if(S_ISDIR(stat_buff->st_mode)){
            printf("dir\n");
        }
        else if(S_ISREG(stat_buff->st_mode))
            printf("file\n");
        else if(S_ISCHR(stat_buff->st_mode))
            printf("char dev\n");
        else if(S_ISBLK(stat_buff->st_mode))
            printf("block dev\n");
        else if(S_ISFIFO(stat_buff->st_mode))
            printf("fifo\n");
        else if(S_ISSOCK(stat_buff->st_mode))
            printf("sock\n");

        char *datetime = ctime(&stat_buff->st_atime);
        printf("\t%-20s %s", "access time:", datetime);
        datetime = ctime(&stat_buff->st_mtime);
        printf("\t%-20s %s", "modification time:", datetime);

    }


    return 0;
}

int main(int argc, char *argv[]){
    
    if(argc > 3){
        if(strcmp(argv[2], "<") == 0) when = S_EARLIER;
        else if(strcmp(argv[2], "=") == 0) when = S_SAME;
        else if(strcmp(argv[2], ">") == 0) when = S_LATER; 
        else error("wrong arguments");

        struct tm *tm = calloc(1, sizeof(struct tm));
        if(sscanf(argv[3], "%d-%d-%d %d:%d", &tm->tm_year, &tm->tm_mon, &tm->tm_mday, &tm->tm_hour, &tm->tm_min) == 5){
            if(tm->tm_min < 0 || tm->tm_min > 59) error("wrong arguments");
            if(tm->tm_hour < 0 || tm->tm_hour > 23) error("wrong arguments");
            tm->tm_year -= 1900;
            tm->tm_mon -= 1;
            tm->tm_sec = 0;
            timestamp = mktime(tm);// - 3600; //timezone
        }
        else error("wrong arguments");  

        if(argv[1][0] == '/'){
            trim_slash(argv[1]);
            nftw(argv[1], trav_fun, 5, FTW_PHYS);
        }
        else{
            char *absolute_path = calloc(200, sizeof(char));
            char *realpath_result = realpath(argv[1], absolute_path);
            if(realpath_result == NULL){
                printf("error: %s\n", argv[1]);
                error("cannot open directory");
            }
            nftw(absolute_path, trav_fun, 5, FTW_PHYS);
            free(absolute_path);
        }
    }
    else error("wrong arguments");
    
    return 0;
}