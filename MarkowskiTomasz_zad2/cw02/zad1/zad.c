#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<ctype.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/times.h>
#include<fcntl.h>
#include<string.h>

char *r0, *r1;
size_t block_size=0;
int file_size=0;

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}

void measure_time(int start){
    static double utime;
    static double stime;
    struct tms time_buff;
    times(&time_buff);
    if(start == 1){
        utime = 1.0*(time_buff.tms_utime + time_buff.tms_cutime) / sysconf(_SC_CLK_TCK);
        stime = 1.0*(time_buff.tms_stime + time_buff.tms_cstime) / sysconf(_SC_CLK_TCK);
    }
    else{
        utime = 1.0*(time_buff.tms_utime + time_buff.tms_cutime) / sysconf(_SC_CLK_TCK) - utime;
        stime = 1.0*(time_buff.tms_stime + time_buff.tms_cstime) / sysconf(_SC_CLK_TCK) - stime;
        printf("user: %.3lf s\n", utime);
        printf("system: %.3lf s\n", stime);
    }
}

void init_buffers(size_t size){
    r0 = calloc(size+1, sizeof(char));  // +1 na nullbyte
    r1 = calloc(size+1, sizeof(char));
    block_size = size;
}
void error(char* msg){
    fprintf(stderr,"%s\n", msg);
    exit(1);
}
void mov_block(char *d, char *s){
    for(int i=0; i<block_size; i++){
        d[i] = s[i];
    }
}



// ** FUNKCJE BIBLIOTECZNE **

void lib_get_block(FILE *fp, int block, char* buff){
    fseek(fp, block*block_size, SEEK_SET);

    size_t bytes_read = fread(buff, 1, block_size, fp);
    if(bytes_read != block_size){
        error("Cannot read blocks");
    }
}

void lib_set_block(FILE *fp, int block, char* buff){
    fseek(fp, block*block_size, SEEK_SET);

    size_t bytes_wrote = fwrite(buff, 1, block_size, fp);
    if(bytes_wrote != block_size){
        error("Cannot write blocks");
    }
}

void lib_sort(FILE *fp){
    //selection sort, w r0 najmniejszy blok
    for(int i=0; i<file_size-1; i++){
        int min_index = i;
        lib_get_block(fp, i, r0);
        for(int j=i+1; j<file_size; j++){
            //szukaj najmniejszego
            lib_get_block(fp, j, r1);
            if((unsigned short)(r1[0]) < (unsigned short)(r0[0])){
                mov_block(r0, r1);
                min_index = j;
            }
        }
        //zamień linie w pliku
        lib_get_block(fp, i, r1);
        lib_set_block(fp, i, r0);
        lib_set_block(fp, min_index, r1);
    }
}

void lib_copy(char *spath, char *dpath){
    FILE *sfp = fopen( spath, "r");
    if(sfp == NULL){
        error("Cannot open source");
    }

    FILE *dfp = fopen( dpath, "w" );
    if(dfp == NULL){
        error("Cannot create file");
    }

    for(int i=0; i< file_size; i++){
        lib_get_block(sfp, i, r0);
        lib_set_block(dfp, i, r0);
    }

    
    if(fclose(sfp) != 0){
        error("Cannot close source");
    }
    if(fclose(dfp) != 0){
        error("Cannot close destination");
    }
}

void lib_file(char *path){
    FILE *fp = fopen(path, "r+");
    if(fp == NULL){
        error("Cannot open file");
    }

    lib_sort(fp);

    //zamykanie pliku
    if(fclose(fp) != 0){
        error("Cannot close file");
    }

}

// ** FUNKCJE SYSTEMOWE **

void sys_get_block(int fd, int block, char* buff){
    lseek(fd, block*block_size, SEEK_SET);

    int bytes_read = read(fd, buff, block_size);
    if(bytes_read != block_size){
        error("Cannot read blocks");
    }
}

void sys_set_block(int fd, int block, char* buff){
    lseek(fd, block*block_size, SEEK_SET);

    int bytes_wrote = write(fd, buff, block_size);
    if(bytes_wrote != block_size){
        error("Cannot write blocks");
    }
}


void sys_sort(int fd){
    //selection sort, w r0 najmniejszy blok
    for(int i=0; i<file_size-1; i++){
        int min_index = i;
        sys_get_block(fd, i, r0);
        for(int j=i+1; j<file_size; j++){
            //szukaj najmniejszego
            sys_get_block(fd, j, r1);
            if((unsigned short)(r1[0]) < (unsigned short)(r0[0])){
                mov_block(r0, r1);
                min_index = j;
            }
        }
        //zamień linie w pliku
        sys_get_block(fd, i, r1);
        sys_set_block(fd, i, r0);
        sys_set_block(fd, min_index, r1);
    }
}


void sys_copy(char *spath, char *dpath){
    int sfd = open( spath, O_RDONLY);
    if(sfd < 0){
        error("Cannot open source");
    }

    int dfd = open( dpath, O_WRONLY|O_CREAT|O_TRUNC );
    if(dfd < 0){
        error("Cannot create file");
    }

    for(int i=0; i< file_size; i++){
        sys_get_block(sfd, i, r0);
        sys_set_block(dfd, i, r0);
    }

    
    if(close(sfd) != 0){
        error("Cannot close source");
    }
    if(close(dfd) != 0){
        error("Cannot close destination");
    }
}

void sys_file(char *path){
    int fd = open(path, O_RDWR);
    if(fd < 0){
        error("Cannot open file");
    }

    sys_sort(fd);

    //zamykanie pliku
    if(close(fd) != 0){
        error("Cannot close file");
    }

}

void generate(char *path){
    int fdrand = open("/dev/urandom", O_RDONLY);
    if(fdrand < 0){
        error("Cannot open urandom");
    }

    int fdfile = open( path, O_WRONLY|O_CREAT|O_TRUNC );
    if(fdfile < 0){
        error("Cannot create file");
    }

    for(int i=0; i<file_size; i++){
        int bytes_read = read(fdrand, r0, block_size);
        if(bytes_read != block_size){
            error("Cannot read blocks");
        }

        
        int bytes_wrote = write(fdfile, r0, block_size);
        if(bytes_wrote != block_size){
            error("Cannot write blocks");
        }

        //printf("%s\n", r0);
    }

    //zamykanie plików
    if(close(fdrand) != 0){
        error("Cannot close urandom");
    }
    if(close(fdfile) != 0){
        error("Cannot close file");
    }

}

int main(int argc, char *argv[]){
    if(argc < 5 || argc > 7){
        error("wrong arguments");
    }
    switch(argc){
        case 5: //GENERATE

            if(strcmp(argv[1], "generate") != 0) error("wrong arguments");
            if(isNum(argv[3]) * isNum(argv[4]) == 0) error("wrong arguments");

            //inicjalizacja
            file_size = atoi(argv[3]);
            init_buffers(atoi(argv[4]));

            generate(argv[2]);
            break;
        case 6: //SORT

            if(strcmp(argv[1], "sort") != 0) error("wrong arguments");
            if(isNum(argv[3]) * isNum(argv[4]) == 0) error("wrong arguments");

            //mozna inicjalizowac
            file_size = atoi(argv[3]);
            init_buffers(atoi(argv[4]));

            if(strcmp(argv[5], "sys") == 0){
                measure_time(1);
                sys_file(argv[2]);
                measure_time(0);
            }
            else if(strcmp(argv[5], "lib") == 0){
                measure_time(1);
                lib_file(argv[2]);
                measure_time(0);
            }
            else error("wrong arguments");
            break;
        case 7: //COPY
            if(strcmp(argv[1], "copy") != 0) error("wrong arguments");
            if(isNum(argv[4]) * isNum(argv[5]) == 0) error("wrong arguments");

            //inicjalizacja
            file_size = atoi(argv[4]);
            init_buffers(atoi(argv[5]));

            if(strcmp(argv[6], "sys") == 0){
                measure_time(1);
                sys_copy(argv[2], argv[3]);
                measure_time(0);
            }
            else if(strcmp(argv[6], "lib") == 0){
                measure_time(1);
                lib_copy(argv[2], argv[3]);
                measure_time(0);
            }
            else error("wrong arguments");
            break;
    }
    return 0;
}