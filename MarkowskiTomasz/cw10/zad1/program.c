#define _DEFAULT_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include<pthread.h>
#include<sys/epoll.h>
#include "text.h"
#include<netinet/in.h>
#include<endian.h>
#define MAX_CLIENTS 10


/* Watki */
pthread_t epoll_thread;
pthread_t cmd_thread;
pthread_t inet_thread;
pthread_t ping_thread;


int max(int a, int b){
    return (a>b)?a:b;
}

/* Mechanizm epoll */
int epoll = -1;

/* Licznik zlecen */
int zlecenie = 0;

/* Tablice z informacjami o klientach */
int cid[MAX_CLIENTS];
int working[MAX_CLIENTS];
int pingi[MAX_CLIENTS];
char *names[MAX_CLIENTS];

/* Liczba obecnie polaczonych klientow */
int curr_conn_client=0;

/* Deskryptory socketów */
int sid;
int isid;

/* Sciezka do socketa, przydatna przy usuwaniu (unlink) w funkcji obslugi sygnalu */
char *argv_path;

/* Wyswietlanie listy klientow */
void print_clients(){
    printf("\nLista klientow:\n");
    for(int i=0; i<curr_conn_client; i++){
        printf("\t%s\n", names[i]);
    }
}

/* Szukanie indeksu klienta po deskryptorze socketu */
int find(int which){
    for(int i=0; i<curr_conn_client; i++){
        if(cid[i] == which) return i;
    }
    return -1;
}

/* Sprawdzanie, czy klient o danej nazwie jest juz na liscie polaczonych klientow */
int alreadyInList(char *name){
    for(int i=0; i<curr_conn_client; i++){
        if(strcmp(names[i], name) == 0) return 1;
    }
    return 0;
}

/* Usuwanie klienta z listy po deskryptorze socketu */
void remove_from_list(int which){
    int where = find(which);
    if(where != -1){
        printf("Usuwanie klienta %s\n", names[where]);

        /* Zsuwanie calej tablicy o jeden w dol od usuwanego elementu */
        curr_conn_client--;
        for(int i=where; i<curr_conn_client; i++){
            working[i] = working[i+1];
            names[i] = names[i+1];
            cid[i] = cid[i+1];
            pingi[i] = pingi[i+1];
        }

        /* Wyrejestrowywanie klienta z mechanizmu epoll */
        struct epoll_event ev;
        ev.events = EPOLLIN|EPOLLET;
        ev.data = (epoll_data_t)which;
        if(epoll_ctl(epoll, EPOLL_CTL_DEL, which, &ev) != 0){perror("Cannot delete from epoll"); exit(1);}
        shutdown(which, SHUT_RDWR);
    }
}

/* Watek odpowiedzialny za pingowanie klientow */
void *ping_thread_routine (void *arg){
    int mode = PING;
    while(1){
        int ilu = curr_conn_client;
        for(int i=0; i<ilu; i++){
            pingi[i] = 0;
            write(cid[i], &mode, sizeof(int));
        }
        sleep(5);
        if(ilu != curr_conn_client) continue; //lista sie zmienila w miedzyczasie
        for(int i=0; i<ilu; i++){
            if(pingi[i] == 0){
                printf("*Brak odpowiedzi na PING od %s*\n", names[i]);
                remove_from_list(cid[i]);
                print_clients();
                break;
            }
        }
    }

    
    return NULL;
}

/* Watek odpowiedzialny za odbieranie komunikatow */
void *epoll_thread_routine(void *arg){
    struct text *text = malloc(sizeof(struct text));
    struct epoll_event eventy;
    while(1){
        int how_many = epoll_wait(epoll, &eventy, 1, 1000);
        if(how_many < 1){
            continue;
        }
        int which = eventy.data.fd;
        int msg_type;
        if(read(which, &msg_type, sizeof(int)) > 0){
            if(msg_type == DISCONNECT){
                printf("Otrzymano zlecenie usuniecia\n");
                remove_from_list(which);
                print_clients();
            }
            else if(msg_type == ANSWER_ZLECENIE){
                recv(which, text, sizeof(struct text), MSG_WAITALL);
                working[find(which)] -= 1;
                printf("Otrzymalem odpowiedz do zlecenia %d o tresci:\n", text->zlecenie);
                printf("%s\n", text->text);
            }
            else if(msg_type == PING){
                pingi[find(which)] = 1;
            }
        }
    }

    /* Dead code */
    free(text);
    return NULL;
}

/* Obsluga SIGINT */
void sigHandler(int signo){
    unlink(argv_path);
    exit(0);
}

/* Wyslij zlecenie do wolnego klienta */
void send_to_free(struct text *text){
    if(curr_conn_client == 0){
        printf("Brak klientow!\n");
        return;
    }
    int mode = MAKE_ZLECENIE;
    for(int i=0; i<curr_conn_client; i++){
        if(working[i] == 0){
            working[i] += 1;
            write(cid[i], &mode, sizeof(int));
            write(cid[i], text, sizeof(struct text));
            return;
        }
    }

    /* A gdy zaden nie jest wolny ... */
    int randomClient = rand()%curr_conn_client;
    working[randomClient] += 1;
    write(cid[randomClient], &mode, sizeof(int));
    write(cid[randomClient], text, sizeof(struct text));
}

/* Watek odpowiedzialny za linie komend i czytanie plikow oraz tworzenie zlecen */
void *cmd_thread_routine(void *arg){
    char path[100];
    struct text *text = malloc(sizeof(struct text));
    while(1){
        scanf("%s", path);
        FILE *plik = fopen(path, "r");
        if(plik > 0){
            
            size_t ile = fread(text->text, 1, MAX_TEXT_SIZE, plik);
            text->text[max(ile+1, MAX_TEXT_SIZE-1)] = 0;
            text->zlecenie = zlecenie++;
            send_to_free(text);
            fclose(plik);            
        }
        else{
            perror("Nie mozna otworzyc pliku");
        }
    }

    free(text);
    return NULL;
}

/* Watek odpowiedzialny za odbieranie komunikatow sieciowych */
void *inet_thread_routine( void *arg ){
    char name_buff[NAME_LEN];
    while(1){
        int newCid = accept(isid, NULL, 0);
        if(newCid != -1){
            printf("Klient INET podlaczony\n");

            recv(newCid, name_buff, NAME_LEN, MSG_WAITALL);
            printf("\to nazwie: %s\n", name_buff);

            int response = 0;

            if(alreadyInList(name_buff)){
                response = 1;
                printf("Taki klient juz istnieje!\n");
                write(newCid, &response, sizeof(int));
                continue;
            }


            cid[curr_conn_client] = newCid;
            strcpy(names[curr_conn_client], name_buff);

            struct epoll_event ev;
            ev.events = EPOLLIN;
            ev.data.fd = newCid;
            if(epoll_ctl(epoll, EPOLL_CTL_ADD, newCid, &ev) != 0){perror("Cannot register epoll event");exit(1);}
            write(newCid, &response, sizeof(int));
            curr_conn_client++;

            print_clients();
        }
    }
    return NULL;
}

int main(int argc, char* argv[]){
    if(argc != 3){printf("Args: port, path\n");exit(1);}
    uint16_t port = atoi(argv[1]);
    argv_path = argv[2];

    /* Konwersja do BigEndian */
    port = htobe16(port);

    srand(time(NULL));
    signal(SIGINT, sigHandler);
    for(int i=0; i<MAX_CLIENTS; i++){
        names[i] = calloc(101, sizeof(char));
        working[i] = 0;
    }

    epoll = epoll_create1(0);

    sid = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sid == -1){perror("Cannot make socket");exit(1);}

    isid = socket(AF_INET, SOCK_STREAM, 0);
    if(isid == -1){perror("Cannot make socket");exit(1);}


    /* Bindowanie socketu lokalnego */
    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, argv[2]);
    if(bind(sid, (struct sockaddr*)&addr, sizeof(addr)) != 0){perror("Cannot bind");exit(1);}

    /* Bindowanie socketu sieciowego */
    struct sockaddr_in iaddr;
    iaddr.sin_family = AF_INET;
    iaddr.sin_port = port;
    iaddr.sin_addr.s_addr = INADDR_ANY;
    if(bind(isid, (struct sockaddr*)&iaddr, sizeof(iaddr)) != 0){perror("Cannot bind inet");exit(1);}

    /* Wlaczenie nasluchiwania */
    listen(sid, MAX_CLIENTS);
    listen(isid, MAX_CLIENTS);

    /* Tworzenie watkow */
    pthread_attr_t tattr;
    pthread_attr_init(&tattr);
    pthread_create(&epoll_thread, &tattr, epoll_thread_routine, NULL);
    pthread_create(&cmd_thread, &tattr, cmd_thread_routine, NULL);
    pthread_create(&inet_thread, &tattr, inet_thread_routine, NULL);
    pthread_create(&ping_thread, &tattr, ping_thread_routine, NULL);

    /* Przyjmowanie polaczen klientow */
    char name_buff[NAME_LEN];
    while(1){
        int newCid = accept(sid, NULL, 0);
        if(newCid != -1){
            printf("Klient podlaczony\n");

            recv(newCid, name_buff, NAME_LEN, MSG_WAITALL);
            printf("\to nazwie: %s\n", name_buff);

            int response = 0;

            if(alreadyInList(name_buff)){
                response = 1;
                printf("Taki klient juz istnieje!\n");
                write(newCid, &response, sizeof(int));
                continue;
            }


            cid[curr_conn_client] = newCid;
            strcpy(names[curr_conn_client], name_buff);

            struct epoll_event ev;
            ev.events = EPOLLIN;
            ev.data.fd = newCid;
            if(epoll_ctl(epoll, EPOLL_CTL_ADD, newCid, &ev) != 0){perror("Cannot register epoll event");exit(1);}
            write(newCid, &response, sizeof(int));
            curr_conn_client++;

            print_clients();
        }
    }


    /* Tez raczej dead code */
    unlink(argv[2]);
    return 0;
}