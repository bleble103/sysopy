#pragma once

#define WORD_LEN 10

typedef struct node {
    struct node *next;
    char *word;
    int count;
} node_t;

typedef struct {
    node_t *root;
} lista_t;

lista_t *new_list(){
    lista_t *res = malloc(sizeof(lista_t));
    res->root = NULL;
    return res;
}

void destroy_node(node_t *node){
    if(node->next != NULL){
        destroy_node(node->next);
    }
    free(node->word);
    free(node);
}
void destroy_list(lista_t *list){
    if(list->root != NULL){
        destroy_node(list->root);
    }
    free(list);
}

node_t *new_node(char *word){
    node_t *res = malloc(sizeof(node_t));
    res->word = malloc(WORD_LEN*sizeof(char));
    strcpy(res->word, word);
    res->count = 1;
    res->next = NULL;
    return res;
}

void insert_node(node_t *node, char *word){
    if(strcmp(node->word, word) == 0){
        node->count += 1;
    }
    else{
        if(node->next == NULL){
            node->next = new_node(word);
        }else{
            insert_node(node->next, word);
        }
    }
}

void insert_list(lista_t *lista, char *word){
    if(lista->root == NULL){
        lista->root = new_node(word);
    }
    else{
        insert_node(lista->root, word);
    }
}