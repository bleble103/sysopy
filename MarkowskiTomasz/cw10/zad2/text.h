#pragma once

#define MAKE_ZLECENIE 1
#define ANSWER_ZLECENIE 2
#define DISCONNECT 3
#define PING 4
#define HELLO 5

#define MAX_TEXT_SIZE 1000
#define NAME_LEN 100

struct text{
    int zlecenie;
    char text[MAX_TEXT_SIZE];
};