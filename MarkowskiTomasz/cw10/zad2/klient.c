#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include "text.h"
#include "list.h"
#include<netinet/in.h>
#include<arpa/inet.h>
#include<endian.h>

int sid;

void sigHandler(int signo){
    int type = DISCONNECT;
    printf("Wysylanie polecenia usuniecia\n");
    write(sid, &type, sizeof(int));
    close(sid);
    exit(0);
}

int main(int argc, char* argv[]){
    signal(SIGINT, sigHandler);
    if(argc != 4){printf("Args: name, [inet/unix], [addr/path]\n");exit(1);}

    

    if(strcmp(argv[2], "unix") == 0){

        /* Socket lokalny */
        sid = socket(AF_UNIX, SOCK_DGRAM, 0);
        if(sid == -1){perror("Cannot make socket");exit(1);}

        int option = 1;
        if(setsockopt(sid, SOL_SOCKET, SO_PASSCRED, &option, sizeof(int)) != 0){perror("Cannot set sockopt");exit(1);}


        struct sockaddr_un addr;
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, argv[3]);

        if(bind(sid, (struct sockaddr*)&addr, sizeof(sa_family_t)) != 0){perror("Cannot connect");exit(1);}
        if(connect(sid, (struct sockaddr*)&addr, sizeof(addr)) != 0){perror("Cannot connect");exit(1);}
    }
    else if(strcmp(argv[2], "inet") == 0){

        /* Socket sieciowy */
        sid = socket(AF_INET, SOCK_DGRAM, 0);
        if(sid == -1){perror("Cannot make socket");exit(1);}

        struct sockaddr_in iaddr;
        iaddr.sin_family = AF_INET;
        for(size_t i=0; i<strlen(argv[3]); i++){
            if(argv[3][i] == ':'){
                argv[3][i] = 0;
                iaddr.sin_port = htobe16(atoi(argv[3]+i+1));
                break;
            }
        }
        inet_aton(argv[3], &iaddr.sin_addr);

        printf("port %d\n", iaddr.sin_port);
        printf("adres %u\n", iaddr.sin_addr.s_addr);

        if(connect(sid, (struct sockaddr*)&iaddr, sizeof(iaddr)) != 0){perror("Cannot connect");exit(1);}
        
        
    }

    
    /* Wyslanie nazwy */
    int mode = HELLO;
    char nazwa[NAME_LEN];
    strcpy(nazwa, argv[1]);
    write(sid, &mode, sizeof(int));
    write(sid, nazwa, NAME_LEN);
    printf("Wyslalem nazwe\n");

    /* Odebranie odpowiedzi */
    int response;
    recv(sid, &response, sizeof(int), MSG_WAITALL);
    if(response){
        printf("Error code: %d\n", response);
        exit(1);
    }
    printf("Connected\n");

    /* Odbieranie komunikatow (glownie zlecen) */
    char text_buff[MAX_TEXT_SIZE];
    struct text *text = malloc(sizeof(struct text));
    int type;
    while(1){
        read(sid, &type, sizeof(int));
        if(type == MAKE_ZLECENIE){
            lista_t *lista = new_list();
            read(sid, text, sizeof(struct text));
            printf("Odebralem zlecenie %d: %s\n",text->zlecenie, text->text);

            printf("Pracuje nad nim...\n");

            strcpy(text_buff, text->text);
            char *pch = strtok(text_buff, " ,.-");
            while(pch != NULL){
                insert_list(lista, pch);
                pch = strtok(NULL, " ,.-");
            }
            
            node_t *node = lista->root;
            size_t ile = 0;
            int slow = 0;
            while(node != NULL){
                ile += sprintf(text->text+ile, "%s: %d\n", node->word, node->count);
                node = node->next;
                slow++;
            }
            sprintf(text->text+ile, "Wszystkich: %d\n", slow);
            
            type = ANSWER_ZLECENIE;
            write(sid, &type, sizeof(int));
            write(sid, text, sizeof(struct text));
            printf("Wyslalem odpowiedz\n");
            destroy_list(lista);

        }
        else if(type == PING){
            printf(" [*]spingowano mnie\n");
            write(sid, &type, sizeof(int));
        }
    }
    free(text);
    return 0;
}