#define _DEFAULT_SOURCE
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include<pthread.h>
#include<sys/epoll.h>
#include "text.h"
#include<netinet/in.h>
#include<endian.h>
#define MAX_CLIENTS 10


/* Watki */
pthread_t epoll_thread;
pthread_t cmd_thread;
pthread_t inet_thread;
pthread_t ping_thread;


int max(int a, int b){
    return (a>b)?a:b;
}

/* Mechanizm epoll */
int epoll = -1;

/* Licznik zlecen */
int zlecenie = 0;

/* Tablice z informacjami o klientach */
int cid[MAX_CLIENTS];
int working[MAX_CLIENTS];
int pingi[MAX_CLIENTS];
char *names[MAX_CLIENTS];
struct sockaddr *adresy[MAX_CLIENTS];
socklen_t lengthy[MAX_CLIENTS];
int deskryptory[MAX_CLIENTS];
socklen_t sockaddrlen;
socklen_t maxsockaddrlen;

/* Liczba obecnie polaczonych klientow */
int curr_conn_client=0;

/* Deskryptory socketów */
int sid;
int isid;

/* Sciezka do socketa, przydatna przy usuwaniu (unlink) w funkcji obslugi sygnalu */
char *argv_path;

/* Wyswietlanie listy klientow */
void print_clients(){
    printf("\nLista klientow:\n");
    for(int i=0; i<curr_conn_client; i++){
        printf("\t%s\n", names[i]);
    }
}


int addr_equals(void *a, socklen_t al, void *b, socklen_t bl){
    if(al != bl) return 0;
    for(int i=0; i<al; i++){
        if(*(char*)a != *(char*)b) return 0;
        a += 1;
        b += 1;
    }
    return 1;
}

/* Szukanie indeksu klienta po adresie socketu */
int dfind(struct sockaddr *addr, socklen_t len){
    //return 0;
    for(int i=0; i<curr_conn_client; i++){
        if(addr_equals(addr, len, adresy[i], lengthy[i])) return i;
    }
    return -1;
}

/* Sprawdzanie, czy klient o danej nazwie jest juz na liscie polaczonych klientow */
int alreadyInList(char *name){
    for(int i=0; i<curr_conn_client; i++){
        if(strcmp(names[i], name) == 0) return 1;
    }
    return 0;
}

void del_from_list(int where){
    curr_conn_client--;
    for(int i=where; i<curr_conn_client; i++){
        working[i] = working[i+1];
        names[i] = names[i+1];
        cid[i] = cid[i+1];
        pingi[i] = pingi[i+1];
        adresy[i] = adresy[i+1];
        lengthy[i] = lengthy[i+1];
        deskryptory[i] = deskryptory[i+1];
    }
}

/* Usuwanie klienta z listy po adresie socketu */
void dremove_from_list(struct sockaddr *addr, socklen_t len){
    int where = dfind(addr, len);
    if(where != -1){
        printf("Usuwanie klienta %s\n", names[where]);

        /* Zsuwanie calej tablicy o jeden w dol od usuwanego elementu */
        del_from_list(where);

    }
}

/* Watek odpowiedzialny za pingowanie klientow */
void *ping_thread_routine (void *arg){
    int mode = PING;
    while(1){
        int ilu = curr_conn_client;
        for(int i=0; i<ilu; i++){
            pingi[i] = 0;
            sendto(deskryptory[i], &mode, sizeof(int), 0, adresy[i], lengthy[i] );
            //write(cid[i], &mode, sizeof(int));
        }
        sleep(5);
        if(ilu != curr_conn_client) continue; //lista sie zmienila w miedzyczasie
        for(int i=0; i<ilu; i++){
            if(pingi[i] == 0){
                printf("*Brak odpowiedzi na PING od %s*\n", names[i]);
                //remove_from_list(cid[i]);
                del_from_list(i);
                print_clients();
                break;
            }
        }
    }

    
    return NULL;
}

/* Watek odpowiedzialny za odbieranie komunikatow */
void *epoll_thread_routine(void *arg){
    struct text *text = malloc(sizeof(struct text));
    struct epoll_event eventy;
    struct sockaddr *recv_addr = malloc(maxsockaddrlen);
    
    char name_buff[NAME_LEN];

    while(1){
        int how_many = epoll_wait(epoll, &eventy, 1, 1000);
        if(how_many < 1){
            continue;
        }
        int which = eventy.data.fd;
        int msg_type;
        if(recvfrom(which, &msg_type, sizeof(int), 0, recv_addr, &sockaddrlen) > 0){
            if(msg_type == DISCONNECT){
                printf("Otrzymano zlecenie usuniecia\n");
                dremove_from_list(recv_addr, sockaddrlen);
                print_clients();
            }
            else if(msg_type == ANSWER_ZLECENIE){
                recv(which, text, sizeof(struct text), MSG_WAITALL);
                working[dfind(recv_addr, sockaddrlen)] -= 1;
                printf("Otrzymalem odpowiedz do zlecenia %d o tresci:\n", text->zlecenie);
                printf("%s\n", text->text);
            }
            else if(msg_type == PING){
                pingi[dfind(recv_addr, sockaddrlen)] = 1;
            }
            else if(msg_type == HELLO){

                printf("Klient podlaczony\n");

                recv(which, name_buff, NAME_LEN, MSG_WAITALL);
                printf("\to nazwie: %s\n", name_buff);

                int response = 0;

                if(alreadyInList(name_buff)){
                    response = 1;
                    printf("Taki klient juz istnieje!\n");
                    sendto(which, &response, sizeof(int), 0, recv_addr, sockaddrlen);
                    continue;
                }


                deskryptory[curr_conn_client] = which;
                adresy[curr_conn_client] = recv_addr;
                lengthy[curr_conn_client] = sockaddrlen;
                strcpy(names[curr_conn_client], name_buff);


                sendto(which, &response, sizeof(int), 0, recv_addr, sockaddrlen);
                recv_addr = malloc(maxsockaddrlen);
                curr_conn_client++;
                

                print_clients();
            }
        }
    }

    /* Dead code */
    free(text);
    return NULL;
}

/* Obsluga SIGINT */
void sigHandler(int signo){
    unlink(argv_path);
    exit(0);
}

/* Wyslij zlecenie do wolnego klienta */
void send_to_free(struct text *text){
    if(curr_conn_client == 0){
        printf("Brak klientow!\n");
        return;
    }
    int mode = MAKE_ZLECENIE;
    for(int i=0; i<curr_conn_client; i++){
        if(working[i] == 0){
            working[i] += 1;
            sendto(deskryptory[i], &mode, sizeof(int), 0, adresy[i], lengthy[i]);
            sendto(deskryptory[i], text, sizeof(struct text), 0, adresy[i], lengthy[i]);
            return;
        }
    }

    /* A gdy zaden nie jest wolny ... */
    int randomClient = rand()%curr_conn_client;
    working[randomClient] += 1;
    sendto(deskryptory[randomClient], &mode, sizeof(int), 0, adresy[randomClient], lengthy[randomClient]);
    sendto(deskryptory[randomClient], text, sizeof(struct text), 0, adresy[randomClient], lengthy[randomClient]);

}

/* Watek odpowiedzialny za linie komend i czytanie plikow oraz tworzenie zlecen */
void *cmd_thread_routine(void *arg){
    char path[100];
    struct text *text = malloc(sizeof(struct text));
    while(1){
        scanf("%s", path);
        FILE *plik = fopen(path, "r");
        if(plik > 0){
            
            size_t ile = fread(text->text, 1, MAX_TEXT_SIZE, plik);
            text->text[max(ile+1, MAX_TEXT_SIZE-1)] = 0;
            text->zlecenie = zlecenie++;
            send_to_free(text);
            fclose(plik);            
        }
        else{
            perror("Nie mozna otworzyc pliku");
        }
    }

    free(text);
    return NULL;
}


int main(int argc, char* argv[]){
    if(argc != 3){printf("Args: port, path\n");exit(1);}
    uint16_t port = atoi(argv[1]);
    argv_path = argv[2];

    /* Konwersja do BigEndian */
    port = htobe16(port);

    srand(time(NULL));
    signal(SIGINT, sigHandler);

    sockaddrlen = max(sizeof(struct sockaddr_in), sizeof(struct sockaddr_un));
    maxsockaddrlen = max(sizeof(struct sockaddr_in), sizeof(struct sockaddr_un));
    for(int i=0; i<MAX_CLIENTS; i++){
        names[i] = calloc(101, sizeof(char));
        working[i] = 0;
        adresy[i] = malloc(sockaddrlen);
    }

    epoll = epoll_create1(0);

    sid = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(sid == -1){perror("Cannot make socket");exit(1);}

    isid = socket(AF_INET, SOCK_DGRAM, 0);
    if(isid == -1){perror("Cannot make socket");exit(1);}


    /* Bindowanie socketu lokalnego */
    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, argv[2]);
    if(bind(sid, (struct sockaddr*)&addr, sizeof(addr)) != 0){perror("Cannot bind");exit(1);}

    /* Bindowanie socketu sieciowego */
    struct sockaddr_in iaddr;
    iaddr.sin_family = AF_INET;
    iaddr.sin_port = port;
    iaddr.sin_addr.s_addr = INADDR_ANY;
    if(bind(isid, (struct sockaddr*)&iaddr, sizeof(iaddr)) != 0){perror("Cannot bind inet");exit(1);}


    /* Tworzenie watkow */
    pthread_attr_t tattr;
    pthread_attr_init(&tattr);
    pthread_create(&epoll_thread, &tattr, epoll_thread_routine, NULL);
    pthread_create(&cmd_thread, &tattr, cmd_thread_routine, NULL);
    pthread_create(&ping_thread, &tattr, ping_thread_routine, NULL);


    /* Rejestracja socketów do epoll */
    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = sid;
    if(epoll_ctl(epoll, EPOLL_CTL_ADD, sid, &ev) != 0){perror("Cannot register epoll event");exit(1);}
    
    ev.data.fd = isid;
    if(epoll_ctl(epoll, EPOLL_CTL_ADD, isid, &ev) != 0){perror("Cannot register epoll event");exit(1);}
            
    while(1){
        sleep(1);
    }


    /* Tez raczej dead code */
    unlink(argv[2]);
    return 0;
}