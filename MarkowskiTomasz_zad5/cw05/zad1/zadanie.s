	.file	"zadanie.c"
	.intel_syntax noprefix
	.text
	.section	.rodata
.LC0:
	.string	"%s\n"
	.text
	.globl	error
	.type	error, @function
error:
.LFB6:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	sub	rsp, 16
	mov	QWORD PTR -8[rbp], rdi
	mov	rax, QWORD PTR stderr[rip]
	mov	rdx, QWORD PTR -8[rbp]
	lea	rsi, .LC0[rip]
	mov	rdi, rax
	mov	eax, 0
	call	fprintf@PLT
	mov	edi, 1
	call	exit@PLT
	.cfi_endproc
.LFE6:
	.size	error, .-error
	.section	.rodata
.LC1:
	.string	"wrong arguments"
.LC2:
	.string	"r"
.LC3:
	.string	"cannot open file"
.LC4:
	.string	"got line ****************"
.LC5:
	.string	"Line %s\n"
.LC6:
	.string	"|"
.LC7:
	.string	" "
.LC8:
	.string	"wrong file"
.LC9:
	.string	"\tparam: %s\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB7:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	push	rbx
	sub	rsp, 136
	.cfi_offset 3, -24
	mov	DWORD PTR -132[rbp], edi
	mov	QWORD PTR -144[rbp], rsi
	mov	esi, 8
	mov	edi, 5
	call	calloc@PLT
	mov	QWORD PTR -64[rbp], rax
	mov	DWORD PTR -20[rbp], 0
	jmp	.L3
.L4:
	mov	eax, DWORD PTR -20[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -64[rbp]
	lea	rbx, [rdx+rax]
	mov	esi, 1
	mov	edi, 100
	call	calloc@PLT
	mov	QWORD PTR [rbx], rax
	add	DWORD PTR -20[rbp], 1
.L3:
	cmp	DWORD PTR -20[rbp], 4
	jle	.L4
	mov	esi, 8
	mov	edi, 5
	call	calloc@PLT
	mov	QWORD PTR -72[rbp], rax
	mov	DWORD PTR -24[rbp], 0
	jmp	.L5
.L6:
	mov	eax, DWORD PTR -24[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	lea	rbx, [rdx+rax]
	mov	esi, 8
	mov	edi, 7
	call	calloc@PLT
	mov	QWORD PTR [rbx], rax
	mov	eax, DWORD PTR -24[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	add	rax, 48
	mov	QWORD PTR [rax], 0
	add	DWORD PTR -24[rbp], 1
.L5:
	cmp	DWORD PTR -24[rbp], 4
	jle	.L6
	mov	QWORD PTR -112[rbp], 200
	mov	rax, QWORD PTR -112[rbp]
	mov	esi, 1
	mov	rdi, rax
	call	calloc@PLT
	mov	QWORD PTR -120[rbp], rax
	mov	esi, 8
	mov	edi, 6
	call	calloc@PLT
	mov	QWORD PTR -80[rbp], rax
	mov	DWORD PTR -28[rbp], 0
	jmp	.L7
.L8:
	mov	eax, DWORD PTR -28[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -80[rbp]
	lea	rbx, [rdx+rax]
	mov	esi, 4
	mov	edi, 2
	call	calloc@PLT
	mov	QWORD PTR [rbx], rax
	mov	eax, DWORD PTR -28[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -80[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	rdi, rax
	call	pipe@PLT
	add	DWORD PTR -28[rbp], 1
.L7:
	cmp	DWORD PTR -28[rbp], 5
	jle	.L8
	cmp	DWORD PTR -132[rbp], 1
	jg	.L9
	lea	rdi, .LC1[rip]
	call	error
.L9:
	mov	rax, QWORD PTR -144[rbp]
	add	rax, 8
	mov	rax, QWORD PTR [rax]
	lea	rsi, .LC2[rip]
	mov	rdi, rax
	call	fopen@PLT
	mov	QWORD PTR -88[rbp], rax
	cmp	QWORD PTR -88[rbp], 0
	jne	.L11
	lea	rdi, .LC3[rip]
	call	error
	jmp	.L11
.L30:
	lea	rdi, .LC4[rip]
	call	puts@PLT
	mov	rbx, QWORD PTR -120[rbp]
	mov	rax, QWORD PTR -120[rbp]
	mov	rdi, rax
	call	strlen@PLT
	sub	rax, 1
	add	rax, rbx
	movzx	eax, BYTE PTR [rax]
	cmp	al, 10
	jne	.L12
	mov	rbx, QWORD PTR -120[rbp]
	mov	rax, QWORD PTR -120[rbp]
	mov	rdi, rax
	call	strlen@PLT
	sub	rax, 1
	add	rax, rbx
	mov	BYTE PTR [rax], 0
.L12:
	mov	DWORD PTR -32[rbp], 0
	mov	rax, QWORD PTR -120[rbp]
	mov	rsi, rax
	lea	rdi, .LC5[rip]
	mov	eax, 0
	call	printf@PLT
	mov	rax, QWORD PTR -120[rbp]
	lea	rsi, .LC6[rip]
	mov	rdi, rax
	call	strtok@PLT
	mov	QWORD PTR -96[rbp], rax
	mov	eax, DWORD PTR -32[rbp]
	lea	edx, 1[rax]
	mov	DWORD PTR -32[rbp], edx
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -64[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	rdx, QWORD PTR -96[rbp]
	mov	rsi, rdx
	mov	rdi, rax
	call	strcpy@PLT
	jmp	.L13
.L14:
	mov	eax, DWORD PTR -32[rbp]
	lea	edx, 1[rax]
	mov	DWORD PTR -32[rbp], edx
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -64[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	rdx, QWORD PTR -96[rbp]
	mov	rsi, rdx
	mov	rdi, rax
	call	strcpy@PLT
.L13:
	lea	rsi, .LC6[rip]
	mov	edi, 0
	call	strtok@PLT
	mov	QWORD PTR -96[rbp], rax
	cmp	QWORD PTR -96[rbp], 0
	jne	.L14
	mov	DWORD PTR -36[rbp], 0
	jmp	.L15
.L29:
	mov	DWORD PTR -40[rbp], 0
	jmp	.L16
.L17:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	edx, DWORD PTR -40[rbp]
	movsx	rdx, edx
	sal	rdx, 3
	lea	rbx, [rax+rdx]
	mov	esi, 1
	mov	edi, 50
	call	calloc@PLT
	mov	QWORD PTR [rbx], rax
	add	DWORD PTR -40[rbp], 1
.L16:
	cmp	DWORD PTR -40[rbp], 5
	jle	.L17
	mov	DWORD PTR -44[rbp], 0
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -64[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	lea	rsi, .LC7[rip]
	mov	rdi, rax
	call	strtok@PLT
	mov	QWORD PTR -104[rbp], rax
	cmp	QWORD PTR -104[rbp], 0
	jne	.L18
	lea	rdi, .LC8[rip]
	call	error
.L18:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rcx, QWORD PTR [rax]
	mov	eax, DWORD PTR -44[rbp]
	lea	edx, 1[rax]
	mov	DWORD PTR -44[rbp], edx
	cdqe
	sal	rax, 3
	add	rax, rcx
	mov	rax, QWORD PTR [rax]
	mov	rdx, QWORD PTR -104[rbp]
	mov	rsi, rdx
	mov	rdi, rax
	call	strcpy@PLT
	jmp	.L19
.L20:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rcx, QWORD PTR [rax]
	mov	eax, DWORD PTR -44[rbp]
	lea	edx, 1[rax]
	mov	DWORD PTR -44[rbp], edx
	cdqe
	sal	rax, 3
	add	rax, rcx
	mov	rax, QWORD PTR [rax]
	mov	rdx, QWORD PTR -104[rbp]
	mov	rsi, rdx
	mov	rdi, rax
	call	strcpy@PLT
.L19:
	lea	rsi, .LC7[rip]
	mov	edi, 0
	call	strtok@PLT
	mov	QWORD PTR -104[rbp], rax
	cmp	QWORD PTR -104[rbp], 0
	jne	.L20
	mov	DWORD PTR -48[rbp], 0
	jmp	.L21
.L22:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	edx, DWORD PTR -48[rbp]
	movsx	rdx, edx
	sal	rdx, 3
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	rsi, rax
	lea	rdi, .LC9[rip]
	mov	eax, 0
	call	printf@PLT
	add	DWORD PTR -48[rbp], 1
.L21:
	mov	eax, DWORD PTR -48[rbp]
	cmp	eax, DWORD PTR -44[rbp]
	jl	.L22
	mov	eax, DWORD PTR -44[rbp]
	mov	DWORD PTR -52[rbp], eax
	jmp	.L23
.L24:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	edx, DWORD PTR -52[rbp]
	movsx	rdx, edx
	sal	rdx, 3
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	rdi, rax
	call	free@PLT
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	edx, DWORD PTR -52[rbp]
	movsx	rdx, edx
	sal	rdx, 3
	add	rax, rdx
	mov	QWORD PTR [rax], 0
	add	DWORD PTR -52[rbp], 1
.L23:
	cmp	DWORD PTR -52[rbp], 5
	jle	.L24
	mov	QWORD PTR -128[rbp], 0
	jmp	.L25
.L26:
	mov	rax, QWORD PTR -128[rbp]
	add	rax, rax
	mov	QWORD PTR -128[rbp], rax
	mov	rax, QWORD PTR -128[rbp]
	shr	rax
	mov	QWORD PTR -128[rbp], rax
	mov	rax, QWORD PTR -128[rbp]
	add	rax, 1
	mov	QWORD PTR -128[rbp], rax
.L25:
	mov	rax, QWORD PTR -128[rbp]
	cmp	rax, 99999999
	jbe	.L26
	call	fork@PLT
	test	eax, eax
	jne	.L27
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -80[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	mov	eax, DWORD PTR [rax]
	mov	esi, 0
	mov	edi, eax
	call	dup2@PLT
	mov	eax, DWORD PTR -32[rbp]
	sub	eax, 1
	cmp	DWORD PTR -36[rbp], eax
	jge	.L28
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	add	rax, 1
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -80[rbp]
	add	rax, rdx
	mov	rax, QWORD PTR [rax]
	add	rax, 4
	mov	eax, DWORD PTR [rax]
	mov	esi, 1
	mov	edi, eax
	call	dup2@PLT
.L28:
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rdx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rdx
	mov	rdx, QWORD PTR [rax]
	mov	eax, DWORD PTR -36[rbp]
	cdqe
	lea	rcx, 0[0+rax*8]
	mov	rax, QWORD PTR -72[rbp]
	add	rax, rcx
	mov	rax, QWORD PTR [rax]
	mov	rax, QWORD PTR [rax]
	mov	rsi, rdx
	mov	rdi, rax
	call	execvp@PLT
.L27:
	add	DWORD PTR -36[rbp], 1
.L15:
	mov	eax, DWORD PTR -36[rbp]
	cmp	eax, DWORD PTR -32[rbp]
	jl	.L29
.L11:
	mov	rdx, QWORD PTR -88[rbp]
	lea	rcx, -112[rbp]
	lea	rax, -120[rbp]
	mov	rsi, rcx
	mov	rdi, rax
	call	getline@PLT
	test	rax, rax
	jg	.L30
	mov	rax, QWORD PTR -88[rbp]
	mov	rdi, rax
	call	fclose@PLT
	mov	eax, 0
	add	rsp, 136
	pop	rbx
	pop	rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	main, .-main
	.ident	"GCC: (Debian 8.2.0-14) 8.2.0"
	.section	.note.GNU-stack,"",@progbits
