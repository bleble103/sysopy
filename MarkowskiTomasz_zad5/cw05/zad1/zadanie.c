#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/unistd.h>
#include<sys/wait.h>


void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int main(int argc, char *argv[]){
    char **programs = calloc(5, sizeof(char*));
    for(int i=0; i<5; i++){
        programs[i] = calloc(100, sizeof(char));
    }
    char ***arguments = calloc(5, sizeof(char**));
    for(int i=0; i<5; i++){
        arguments[i] = calloc(7, sizeof(char*));//7 bo pierwszy to program, ostatni NULL (wartownik)
        
        arguments[i][6] = NULL;
    }
    size_t line_size = 200;
    char *line_buff = calloc(line_size, sizeof(char));
    char *line_iter;
    char *param_iter;
    int children_no = 0;

    int **fd = calloc(6, sizeof(int*));
    for(int i=0; i<6; i++){
        fd[i] = calloc(2, sizeof(int));
    }


    if(argc < 2) error("wrong arguments");
    
    FILE *lista = fopen(argv[1], "r");
    if(lista == NULL) error("cannot open file");
    //int prog = 0;
    while(getline(&line_buff, &line_size, lista) > 0){
        for(int i=0; i<6; i++){
            pipe(fd[i]);
        }

        printf("got line ****************\n");
        if(line_buff[strlen(line_buff) - 1] == '\n'){
            line_buff[strlen(line_buff) - 1] = 0; //remove \n
        
        }


        int prog = 0;
        printf("Line %s\n", line_buff);
        line_iter = strtok(line_buff, "|");
        strcpy(programs[prog++], line_iter);
        while((line_iter = strtok(NULL, "|")) != NULL){
            strcpy(programs[prog++], line_iter);
        }
        for(int i=0; i<prog; i++){
            for(int j=0; j<6; j++){ 
                arguments[i][j] = calloc(50, sizeof(char));
            }

            int param = 0;
            //printf("prog: %s\n", programs[i]);
            param_iter = strtok(programs[i], " ");
            if(param_iter == NULL) error("wrong file");
            strcpy(arguments[i][param++], param_iter);
            while((param_iter = strtok(NULL, " ")) != NULL){
                strcpy(arguments[i][param++], param_iter);
            }
            for(int j=0; j<param; j++){
                printf("\tparam: %s\n", arguments[i][j]);
               
                //printf("\tprog: %s\n", programs[i]);
            }
            for(int j=param; j<6; j++){
                free(arguments[i][j]);
                arguments[i][j] = NULL;
            }
            /*
            for(volatile long long unsigned int k=0ULL; k<100000000ULL; k++){
                k*=2;
                k/=2;
            }
            */
            if(fork() == 0){
                //child
                ++children_no;

                dup2(fd[i][0], STDIN_FILENO);
								close(fd[i][1]);
                if(i < prog-1){
                    dup2(fd[i+1][1], STDOUT_FILENO);
										close(fd[i+1][0]);
                    //printf("Change stdout for %s\n", arguments[i][0]);
                    //close(fd[i+1][0]);
                }
                //close(fd[i][1]);
                execvp(arguments[i][0], arguments[i]);
            }
        }

        for(int i=0; i<6; i++){
            close(fd[i][0]);
            close(fd[i][1]);
        }

    }

    fclose(lista);

    while(children_no-- > 0) wait(NULL);

    return 0;
}
