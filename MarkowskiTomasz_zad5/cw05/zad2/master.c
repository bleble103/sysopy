#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>


int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}
void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int fifo_fd;
char *fifo_name;

void sigint_handler(){
    printf("\nclosing fifo...\n");
    close(fifo_fd);
    printf("removing fifo...\n");
    unlink(fifo_name);
    exit(0);
}


int main(int argc, char *argv[]){
    char buff[100];
    if(argc != 2) error("wrong arguments");

    /* CTRL+C */
    signal(SIGINT, sigint_handler);
    
    fifo_name = argv[1];
    if(mkfifo(argv[1], S_IRWXU) != 0) error("cannot create fifo");
    fifo_fd = open(argv[1], O_RDONLY);
    if(fifo_fd < 0) error("cannot open fifo");
    while(1){
        while(read(fifo_fd, buff, 100) > 0){
            printf("%s", buff);
        }
    }
    

    unlink(argv[1]); //rm fifo
    return 0;
}
