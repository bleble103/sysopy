#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}
void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}


int main(int argc, char *argv[]){
    if(argc != 3 || !isNum(argv[2])) error("wrong arguments");
    int msg_no = atoi(argv[2]);

    int fifo_fd = open(argv[1], O_WRONLY);
    if(fifo_fd < 0) error("cannot open fifo");


    char *buff;
    char wpis[100];
    printf("PID: %d\n", getpid());
    while(msg_no--){
        sleep(rand()%4+2);
        buff = calloc(100, sizeof(char));
        for(int i=0; i<100; i++) wpis[i] = 0;
        sprintf(wpis, "PID: %d - ", getpid());

        FILE *wynik = popen("date", "r");
        fread(buff, 1, 100, wynik);
        strcat(wpis, buff);
        write(fifo_fd, wpis, 100);

        fclose(wynik);
        free(buff);
    }

    close(fifo_fd);
    return 0;
}
