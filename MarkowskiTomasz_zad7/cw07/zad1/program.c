#include<stdio.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<stdlib.h>
#include<semaphore.h>
#include<fcntl.h>

#define SEM_WRITE 0
#define SEM_READ 1

//semaphore id
int semid;

void sem_val(int which, int val){
    static struct sembuf oper;
    oper.sem_num = which;
    oper.sem_op = val;
    oper.sem_flg = 0;
    semop(semid, &oper, 1);
}

int main(int argc, char *argv[]){

    key_t klucz = ftok("/root/",'a');
    printf("%d\n", klucz);

    /* Tworzenie shared memory */
    size_t mem_size = sizeof(int);
    int shmid = shmget(klucz, mem_size, IPC_CREAT);
    if(shmid < 0) {perror("Cannot create shm");exit(1);}

    /* Przylaczanie segmentu shm */
    int *common = shmat(shmid, NULL, 0);
    if(common == (void*)-1) {perror("Cannot attach shared memory");exit(1);}

    /* Tworzenie 2 semaforów */
    semid = semget(klucz, 2, IPC_CREAT);
    if(semid == -1) {perror("Cannot create semaphores");exit(1);}

    /* Inicjalizacja semaforów */
    if(semctl(semid, SEM_READ, SETVAL, 0) == -1){perror("Cannot init sem");exit(1);}
    if(semctl(semid, SEM_WRITE, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}

    if(fork() == 0){
        //dziecko

        /* Podpinanie shared memory */
        //if(common == (void*)-1) {perror("Cannot attach shared memory in child");exit(1);}


        for(int i=0; i<500; i++){

            /* Zabierz semafor */
            sem_val(SEM_READ, -1);
            printf("dziecko wchodzi do sekcji krytycznej\n");

            printf("common z dziecka = %d\n", *common);

            /* Oddaj semafor */
            printf("dziecko wychodzi z sekcji krytycznej\n");
            sem_val(SEM_WRITE, 1);
        }

        /* Odlaczanie shared memory */
        if(shmdt(common) == -1) {perror("Cannot detach shared memory in child");exit(1);}

        exit(0);
        
    }
    else{
        //rodzic


        for(int i=0; i<500; i++){
            /* Zabierz semafor */
            sem_val(SEM_WRITE, -1);
            printf("rodzic wchodzi do sekcji krytycznej\n");

            *common = i;
            printf("rodzic ustawia common = %d\n", i);
            //sleep(1);

            /* Oddaj semafor */
            printf("rodzic wychodzi z sekcji krytycznej\n");
            sem_val(SEM_READ, 1);
        }
        //sleep(2);
    }

    /* Czekanie na dziecko */
    wait(NULL);

    /* Odlaczanie shared memory */
    if(shmdt(common) == -1) {perror("Cannot detach shared memory");}

    /* Usuwanie shared memory */
    shmctl(shmid, IPC_RMID, NULL);

    /* Zamykanie semafora */
    if(semctl(semid, 0, IPC_RMID, 0) == -1){perror("Cannot close sem");}

    return 0;
}