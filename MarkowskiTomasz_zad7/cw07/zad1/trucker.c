#define _XOPEN_SOURCE 800

#include<stdio.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<time.h>
#include<unistd.h>
#include<stdlib.h>
#include<semaphore.h>
#include<fcntl.h>
#include<signal.h>
#include "tasma.h"
#include "sem.h"


/* Ladownosc ciezarowki */
int X;
int currentX;

/* Max ilosc paczek na tasmie */
int K;

/* Ladownosc tasmy */
int M;

/* Flaga petli */
int working = 1;
int stopTasma = 0;

/* Obsluga sygnalu */
void sigHandler(int signo){
    working = 0;

    printf("\n**** Konczenie pracy ciezarowki, ladowanie pozostalych paczek ****\n");

    /* Blokada tasmy */
    if(fork() == 0){
        sem_delta(semid, SEM_QUERY, -1);
        exit(0);
    }
    //sem_delta(semid, SEM_QUERY, -1);
}

int main(int argc, char* argv[]){
    
    /* Argumenty */
    if(argc != 4){printf("Args\n");exit(1);}
    X = atoi(argv[1]);
    K = atoi(argv[2]);
    M = atoi(argv[3]);

    /* Obsluga sygnalu */
    signal(SIGINT, sigHandler);

    // /* Tasma */
    // struct tasma tasma;
    // initTasma(&tasma, K, M);
    // printf("Rozmiar tasmy: %lu B\n", sizeof(tasma));


    key_t klucz = ftok("/root/",'a');
    printf("%d\n", klucz);

    /* Tworzenie shared memory */
    size_t mem_size = sizeof(tasma);
    int shmid = shmget(klucz, mem_size, IPC_CREAT|0666);
    if(shmid < 0) {perror("Cannot create shm");exit(1);}

    /* Przylaczanie segmentu shm */
    tasma = (struct tasma*)shmat(shmid, NULL, 0);
    if(tasma == (void*)-1) {perror("Cannot attach shared memory");exit(1);}

    /* Inicjalizowanie tasmy */
    initTasma(tasma, K, M);

    /* Tworzenie SEM_NO semaforów */
    semid = semget(klucz, SEM_NO, IPC_CREAT|0666);
    if(semid == -1) {perror("Cannot create semaphores");exit(1);}

    /* Inicjalizacja semaforów */
    if(semctl(semid, SEM_SIZE, SETVAL, K) == -1){perror("Cannot init sem");exit(1);}
    if(semctl(semid, SEM_WEIGHT, SETVAL, M) == -1){perror("Cannot init sem");exit(1);}
    if(semctl(semid, SEM_SHM, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}
    if(semctl(semid, SEM_QUERY, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}

    /* Ilosc wolnych miejsc w ciezarowce */
    currentX = X;

    while(1){
        /* Pelna ciezarowka */
        if(currentX == 0){
            printf("Ciezarowka pelna odjezdza...\n");
            sleep(3);
            printf("Nowa ciezarowka podstawiona\n");

            currentX = X;
        }

        sem_delta(semid, SEM_SHM, -1);
        if(tasma->index > 0){
            sem_delta(semid, SEM_SIZE, 1);
            sem_delta(semid, SEM_WEIGHT, tasma->paczki[0].masa);
            printf("Zaladowano paczke o masie %d od PID: %d\n", tasma->paczki[0].masa, tasma->paczki[0].pid);
            tasma->masa += tasma->paczki[0].masa;
            tasma->miejsca += 1;
            currentX -= 1;
            printf("\tCzekala: %lf s\n",dokladnyCzas()-tasma->paczki[0].czas);
            printf("\tWolnych: %d, zajetych: %d\n", currentX, X-currentX);
            printf("\t%lf\n\n", dokladnyCzas());
            takeTasma(tasma);
        }
        else{
            /* Koniec pracy */
            if(working == 0){
                break;
            }
        }

        sem_delta(semid, SEM_SHM, 1);
        struct timespec ts;
        ts.tv_sec=0;
        ts.tv_nsec = 500000000;
        nanosleep(&ts, NULL);
        //sleep(1);
    }

    printf("\nExiting...\n");
    
    /* Odlaczanie shared memory */
    if(shmdt(tasma) == -1) {perror("Cannot detach shared memory");}

    /* Usuwanie shared memory */
    shmctl(shmid, IPC_RMID, NULL);

    /* Zamykanie semafora */
    if(semctl(semid, 0, IPC_RMID, 0) == -1){perror("Cannot close sem");}

    return 0;
}