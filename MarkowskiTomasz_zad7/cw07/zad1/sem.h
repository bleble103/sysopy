#pragma once

#include<semaphore.h>

#define SEM_SIZE 0
#define SEM_WEIGHT 1
#define SEM_SHM 2
#define SEM_QUERY 3
#define SEM_NO 4

int sem_delta(int semid, int which, int val){
    static struct sembuf oper;
    oper.sem_num = which;
    oper.sem_op = val;
    oper.sem_flg = 0;
    return semop(semid, &oper, 1);
}

int sem_delta_nowait(int semid, int which, int val){
    static struct sembuf oper;
    oper.sem_num = which;
    oper.sem_op = val;
    oper.sem_flg = IPC_NOWAIT;
    return semop(semid, &oper, 1);
}

//semaphore id
int semid;