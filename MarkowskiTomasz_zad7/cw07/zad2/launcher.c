#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc, char *argv[]){
    if(argc < 2){printf("Args...\n");exit(1);}
    int maxN = atoi(argv[1]);
    for(int i=1; i<= maxN; i++){
        if(fork() == 0){
            char buff[5];
            sprintf(buff, "%d", i);

            execlp("./loader","./loader",buff, NULL);
            exit(0);
        }
    }
    return 0;
}