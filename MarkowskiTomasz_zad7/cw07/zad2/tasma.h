#pragma once
#define MAX_PACZKI 100


/* Paczka na tasmie */
struct node{
    int masa;

    /* PID pracownika */
    pid_t pid;

    /* Czas w milisekundach */
    double czas;
};

struct tasma{
    /* Wolne miejsca */
    int miejsca;

    /* Wolna masa */
    int masa;

    /* Paczki */
    struct node paczki[MAX_PACZKI];

    /* wskaznik dla nastepnej paczki */
    int index;

};

void initTasma(struct tasma *tasma, int K, int M){
    tasma->miejsca = K;
    tasma->masa = M;
    tasma->index = 0;
}

void takeTasma(struct tasma *tasma){
    for(int i=0; i<tasma->index-1; i++){
        tasma->paczki[i] = tasma->paczki[i+1];
    }
    tasma->index --;
}

struct tasma *tasma;

double dokladnyCzas(){
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        return ts.tv_sec + ts.tv_nsec*1e-9;
}