#define _XOPEN_SOURCE 800

#include<stdio.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<time.h>
#include<unistd.h>
#include<stdlib.h>
#include<semaphore.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include "tasma.h"
#include "sem.h"

#define MAX_LOADERS 100

/* Ladownosc ciezarowki */
int X;
int currentX;

/* Max ilosc paczek na tasmie */
int K;

/* Ladownosc tasmy */
int M;

/* Flaga petli */
int working = 1;
int stopTasma = 0;

/* PIDy loaderów */
pid_t loaders[MAX_LOADERS];

/* Obsluga sygnalu */
void sigHandler(int signo){
    working = 0;

    printf("\n**** Konczenie pracy ciezarowki, ladowanie pozostalych paczek ****\n");

    /* Blokada tasmy */
    if(fork() == 0){
        sem_delta(semid, SEM_QUERY, -1);
        exit(0);
    }
    //sem_delta(semid, SEM_QUERY, -1);
}

void pidHandler(int signo, siginfo_t *sinfo, void *ctx){
    int i=0;
    while(loaders[i] != 0) i++;
    loaders[i] = sinfo->si_value.sival_int;
    printf("Received loader PID: %d\n", sinfo->si_value.sival_int);
}

int main(int argc, char* argv[]){
    printf("My PID: %d\n", getpid());

    /* Inicjalizowanie PIDow loaderow */
    for(int i=0; i<MAX_LOADERS; i++) loaders[i] = 0;
    
    /* Argumenty */
    if(argc != 4){printf("Args\n");exit(1);}
    X = atoi(argv[1]);
    K = atoi(argv[2]);
    M = atoi(argv[3]);

    /* Obsluga sygnalu */
    signal(SIGINT, sigHandler);

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = pidHandler;
    sigaction(SIGUSR1, &sa, NULL);

    // /* Tasma */
    // struct tasma tasma;
    // initTasma(&tasma, K, M);
    // printf("Rozmiar tasmy: %lu B\n", sizeof(tasma));


    key_t klucz = ftok("/root/",'a');
    printf("%d\n", klucz);

    /* Tworzenie shared memory */
    size_t mem_size = sizeof(tasma);
    int shmid = shm_open("/tasma", O_CREAT|O_RDWR, 0666);
    if(shmid < 0) {perror("Cannot create shm");exit(1);}
    if(ftruncate(shmid, mem_size) != 0){perror("Cannot truncate shm");exit(1);}
    //int shmid = shmget(klucz, mem_size, IPC_CREAT|0666);

    /* Przylaczanie segmentu shm */
    tasma = (struct tasma*)mmap(NULL, mem_size, PROT_READ|PROT_WRITE, MAP_SHARED, shmid, 0);
    if(tasma == (void*)-1) {perror("Cannot attach shared memory");exit(1);}

    /* Inicjalizowanie tasmy */
    initTasma(tasma, K, M);

    /* Tworzenie SEM_NO semaforów wraz z inicjalizacją */
    semid[SEM_SIZE] = sem_open("/sem_size", O_CREAT|O_RDWR, 0666, K);
    semid[SEM_WEIGHT] = sem_open("/sem_weight", O_CREAT|O_RDWR, 0666, M);
    semid[SEM_SHM] = sem_open("/sem_shm", O_CREAT|O_RDWR, 0666, 1);
    semid[SEM_QUERY] = sem_open("/sem_query", O_CREAT|O_RDWR, 0666, 1);
    sem_open("/sem_pid", O_CREAT|O_WRONLY, 0666, getpid());
    //semid = semget(klucz, SEM_NO, IPC_CREAT|0666);
    for(int i=0; i<SEM_NO; i++){
        if(semid[i] == SEM_FAILED) {perror("Cannot create semaphores");exit(1);}
    }

    /* Inicjalizacja semaforów */
    //if(semctl(semid, SEM_SIZE, SETVAL, K) == -1){perror("Cannot init sem");exit(1);}
    //if(semctl(semid, SEM_WEIGHT, SETVAL, M) == -1){perror("Cannot init sem");exit(1);}
    //if(semctl(semid, SEM_SHM, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}
    //if(semctl(semid, SEM_QUERY, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}

    /* Ilosc wolnych miejsc w ciezarowce */
    currentX = X;

    while(1){
        /* Pelna ciezarowka */
        if(currentX == 0){
            printf("Ciezarowka pelna odjezdza...\n");
            sleep(3);
            printf("Nowa ciezarowka podstawiona\n");

            currentX = X;
        }

        sem_delta(semid, SEM_SHM, -1);
        if(tasma->index > 0){
            sem_delta(semid, SEM_SIZE, 1);
            sem_delta(semid, SEM_WEIGHT, tasma->paczki[0].masa);
            printf("Zaladowano paczke o masie %d od PID: %d\n", tasma->paczki[0].masa, tasma->paczki[0].pid);
            tasma->masa += tasma->paczki[0].masa;
            tasma->miejsca += 1;
            currentX -= 1;
            printf("\tCzekala: %lf s\n",dokladnyCzas()-tasma->paczki[0].czas);
            printf("\tWolnych: %d, zajetych: %d\n", currentX, X-currentX);
            printf("\t%lf\n\n", dokladnyCzas());
            takeTasma(tasma);
        }
        else{
            /* Koniec pracy */
            if(working == 0){
                break;
            }
        }

        sem_delta(semid, SEM_SHM, 1);
        struct timespec ts;
        ts.tv_sec=0;
        ts.tv_nsec = 500000000;
        nanosleep(&ts, NULL);
        //sleep(1);
    }

    printf("\nExiting...\n");
    
    /* Informowanie loaderow o koncu pracy */
    for(int i=0; (loaders[i] != 0) && i<MAX_LOADERS; i++){
        printf("Killing PID: %d\n", loaders[i]);
        kill(loaders[i], SIGINT);
    }

    sleep(1);

    printf("Cleaning IPC...\n");

    /* Odlaczanie shared memory */
    if(munmap(tasma, mem_size) == -1) {perror("Cannot detach shared memory");}

    /* Usuwanie shared memory */
    shm_unlink("/tasma");
    //shmctl(shmid, IPC_RMID, NULL);

    /* Zamykanie semafora */
    for(int i=0; i<SEM_NO; i++){
        if(sem_close(semid[i]) == -1){perror("Cannot close sem");}
    }
    if(sem_unlink("/sem_size") == -1){perror("Cannot delete sem");}
    if(sem_unlink("/sem_weight") == -1){perror("Cannot delete sem");}
    if(sem_unlink("/sem_shm") == -1){perror("Cannot delete sem");}
    if(sem_unlink("/sem_query") == -1){perror("Cannot delete sem");}
    sem_unlink("/sem_pid");

    return 0;
}