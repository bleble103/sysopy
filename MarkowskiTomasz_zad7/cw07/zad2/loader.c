#define _XOPEN_SOURCE 800
#include<stdio.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<stdlib.h>
#include<semaphore.h>
#include<fcntl.h>
#include<signal.h>
#include<time.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include "tasma.h"
#include "sem.h"


/* Ladownosc paczki */
int X;

/* Flaga petli */
int working = 1;
int cycles = -1;

/* Obsluga sygnalu */
void sigHandler(int signo){
    working = 0;
}

/* Spanie przed kolejnym polozeniem paczki */
void sleepRandom(){
    static struct timespec ts;
    ts.tv_sec=rand()%2;
    ts.tv_nsec = 500000000 + 100000000*(rand()%4);
    nanosleep(&ts, NULL);
}

/* PID truckera */
pid_t trucker_pid;

int main(int argc, char* argv[]){
    
    /* Argumenty */
    if(argc >= 2){
        X = atoi(argv[1]);
    }
    else{
        printf("Args\n");
        exit(1);
    }

    /* Obsluga sygnalu */
    signal(SIGINT, sigHandler);

    // /* Tasma */
    // struct tasma tasma;
    // initTasma(&tasma, K, M);
    // printf("Rozmiar tasmy: %lu B\n", sizeof(tasma));


    key_t klucz = ftok("/root/",'a');
    printf("%d\n", klucz);

    /* Tworzenie shared memory */
    size_t mem_size = sizeof(tasma);
    int shmid = shm_open("/tasma", O_RDWR, 0666);
    if(shmid < 0) {perror("Cannot open shm");exit(1);}
    //if(ftruncate(shmid, mem_size) != 0){perror("Cannot truncate shm");exit(1);}
    //int shmid = shmget(klucz, mem_size, IPC_CREAT|0666);

    /* Przylaczanie segmentu shm */
    tasma = (struct tasma*)mmap(NULL, mem_size, PROT_READ|PROT_WRITE, MAP_SHARED, shmid, 0);
    if(tasma == (void*)-1) {perror("Cannot attach shared memory");exit(1);}

    /* Inicjalizowanie tasmy */
    //initTasma(tasma, K, M);

    /* Tworzenie SEM_NO semaforów */
    //semid = semget(klucz, SEM_NO, 0);
    //if(semid == -1) {perror("Cannot create semaphores");exit(1);}

    /* Inicjalizacja semaforów */
    semid[SEM_SIZE] = sem_open("/sem_size", O_RDWR);
    semid[SEM_WEIGHT] = sem_open("/sem_weight", O_RDWR);
    semid[SEM_SHM] = sem_open("/sem_shm", O_RDWR);
    semid[SEM_QUERY] = sem_open("/sem_query", O_RDWR);
    //semid = semget(klucz, SEM_NO, IPC_CREAT|0666);
    for(int i=0; i<SEM_NO; i++){
        if(semid[i] == SEM_FAILED) {perror("Cannot create semaphores");exit(1);}
    }

    /* Cheat na PID truckera */
    sem_t *sem_pid = sem_open("/sem_pid", O_RDONLY);
    if(sem_getvalue(sem_pid, &trucker_pid) != 0){perror("Cannot get trucker's pid");exit(1);}
    printf("PID truckera: %d\n", trucker_pid);

    /* Wyslanie swojego PIDa do truckera */
    if(sigqueue(trucker_pid, SIGUSR1, (union sigval)getpid()) != 0){perror("Cannot send pid to trucker");exit(1);}

    //if(semctl(semid, SEM_SIZE, SETVAL, K) == -1){perror("Cannot init sem");exit(1);}
    //if(semctl(semid, SEM_WEIGHT, SETVAL, M) == -1){perror("Cannot init sem");exit(1);}
    //if(semctl(semid, SEM_SHM, SETVAL, 1) == -1){perror("Cannot init sem");exit(1);}

    int waiting = 0;

    while(working == 1){
        printf("[%d] ", getpid());
        double czas = dokladnyCzas();

        if(sem_delta_nowait(semid, SEM_QUERY, -1) != 0){
            waiting = 1;
            printf("Czekam na tasme...\n");
            if(sem_delta(semid, SEM_QUERY, -1) != 0){
                /* Koniec ciezarowki */
                printf("Koniec ciezarowki\n");
                exit(0);
            }
        }
        if( (sem_delta_nowait(semid, SEM_WEIGHT, -X) != 0) ){
            if(waiting == 0){
                waiting = 1;
                printf("Czekam na tasme...\n");
            }
            sem_delta(semid, SEM_WEIGHT, -X);
        }
       
        if(sem_delta_nowait(semid, SEM_SIZE, -1) != 0){
            if(waiting == 0){
                printf("Czekam na tasme...\n");
                waiting = 1;
            }
            sem_delta(semid, SEM_SIZE, -1);
        }
        sem_delta(semid, SEM_SHM, -1);
        
        tasma->paczki[tasma->index].pid = getpid();
        tasma->paczki[tasma->index].masa = X;
        tasma->paczki[tasma->index].czas = czas;
        tasma->index ++;
        tasma->masa -= X;
        tasma->miejsca -= 1;
        printf("[%d] Polozylem paczke\n", getpid());
        printf("\tZostalo miejsc: %d, masy: %d\n", tasma->miejsca, tasma->masa);
        printf("\t%lf\n", dokladnyCzas());
        if(waiting == 1){
            printf("\tCzekalem na tasme %lf s\n", dokladnyCzas() - czas);
        }
        printf("\n");
        waiting = 0;

        sem_delta(semid, SEM_SHM, 1);
        sem_delta(semid, SEM_QUERY, 1);

        if(cycles > 0){
            cycles--;
            if(cycles == 0) break;
        }

        sleepRandom();
        //sleep(1);
    }

    printf("\nExiting...\n");
    
    /* Odlaczanie shared memory */
    if(munmap(tasma, mem_size) == -1) {perror("Cannot detach shared memory");}

    /* Usuwanie shared memory */
    //shmctl(shmid, IPC_RMID, NULL);

    /* Zamykanie semafora */
    for(int i=0; i<SEM_NO; i++){
        if(sem_close(semid[i]) == -1){perror("Cannot close sem");}
    }
    //if(semctl(semid, 0, IPC_RMID, 0) == -1){perror("Cannot close sem");}

    return 0;
}