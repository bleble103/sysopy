#pragma once

#include<semaphore.h>

#define SEM_SIZE 0
#define SEM_WEIGHT 1
#define SEM_SHM 2
#define SEM_QUERY 3
#define SEM_NO 4

sem_t *semid[SEM_NO];

int sem_delta(sem_t **semid, int which, int val){
    int res;
    while(val > 0){
        res = sem_post(semid[which]);
        val--;
    }
    while(val < 0){
        res = sem_wait(semid[which]);
        val++;
    }
    return res;
}

int sem_delta_nowait(sem_t **semid, int which, int val){
    int res;
    int successes = 0;
    while(val < 0){
        res = sem_trywait(semid[which]);
        if(res == -1){
            for(int i=0; i<successes; i++){
                sem_post(semid[which]);
            }
            return res;
        }
        else{
            successes++;
        }
        val--;
    }
    return res;
}

//semaphore id
//int semid;