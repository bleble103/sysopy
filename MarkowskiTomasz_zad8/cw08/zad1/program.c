#define _XOPEN_SOURCE 750
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<string.h>
#include<time.h>
#include<stdint.h>
#include<math.h>
// #define DEBUG

double pobierz_sekundy(){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + 1e-9*ts.tv_nsec;
}

enum method{
    BLOCK,
    INTERLEAVED
};


int threads_no;
enum method method;
char *input_name;
char *filter_name;
char *output_name;

uint8_t **picture;
int width;
int height;
uint8_t **result_picture;
int colors;

double **filter;
int filter_size;

int max(int a, int b){
    if(a > b) return a;
    return b;
}


void generate_pixel(int x, int y){
    double res = 0;
    for(int i=0; i<filter_size; i++){
        for(int j=0; j<filter_size; j++){
            res += filter[i][j]*picture[max(0, x-ceil(filter_size/2.0)+i-1)][max(0, y-ceil(filter_size/2.0)+j-1)];
        }
    }
    result_picture[x][y] = res+0.5; //round
}

void *thread_routine(void *arg){

    /* Pobranie argumentu */
    int val = *(int*)arg;

    #ifdef DEBUG
    printf("\t[THREAD] I'm thread\n\treceived arg: %d\n\tmy thread_id: %lu\n\n", val, pthread_self());
    #endif

    /* Poczatek pomiaru czasu */
    double *czas = malloc(sizeof(double));
    *czas = pobierz_sekundy();
    

    /* Filtrowanie obrazka */
    if(method == BLOCK){
        for(int i=(val-1)*ceil(height/threads_no); i<val*ceil(height/threads_no); i++){
            for(int j=0; j<width; j++){
                generate_pixel(i, j);
            }
        }
    }
    else if(method == INTERLEAVED){
        for(int i=val-1; i<height; i+= threads_no){
            for(int j=0; j<width; j++){
                generate_pixel(i, j);
            }
        }
    }

    /* Koniec pomiaru czasu */
    *czas = pobierz_sekundy() - *czas;

    /* Zakonczenie watku */
    #ifdef DEBUG
    printf("\t[THREAD] End of thread\n");
    #endif

    return czas;
}

void *safe_malloc(size_t size){
    void *result = malloc(size);
    if(result == NULL){perror("Cannot alloc memory");exit(1);}
    return result;
}


int main(int argc, char *argv[]){

    /* Argumenty */
    if(argc != 6){printf("Args: threads_no, block/interleaved, input_file, filter_file, output_file\n");exit(1);}
    threads_no = atoi(argv[1]);
    if(threads_no <= 0){printf("Wrong threads_no\n");exit(1);}
    if(strcmp(argv[2], "block") == 0) method = BLOCK;
    else if (strcmp(argv[2], "interleaved") == 0) method = INTERLEAVED;
    else{printf("Wrong method\n");exit(1);}
    input_name = argv[3];
    filter_name = argv[4];
    output_name = argv[5];




    /* Czyszczenie surowego wejscia z komentarzy */
    FILE *raw_input_file = fopen(input_name, "r");
    if(raw_input_file == NULL){perror("Cannot open input file");exit(1);}

    FILE *input_file = fopen(".temp", "w+");
    if(input_file == NULL){perror("Cannot open temp file");exit(1);}

    char *line;
    size_t line_size = 255;
    line = malloc(line_size*sizeof(char));
    size_t read_chars = 0;
    while( (read_chars = getline(&line, &line_size, raw_input_file)) != -1){
        if(line[0] == '#') continue;
        fwrite(line, 1, read_chars, input_file);
    }
    fseek(input_file, SEEK_SET, 0);



    /* Otwarcie pliku obrazu i zczytanie jego wlasciwosci */
    //FILE *input_file = fopen(input_name, "r");
    if(input_file == NULL){perror("Cannot open input file");exit(1);}

    /* Pobranie formatu */
    char file_format[3];
    file_format[2] = 0; //nullbyte
    for(int i=0; i<2; i++){
        fscanf(input_file, "%c", &file_format[i]);
    }
    fscanf(input_file, "%d", &width);
    fscanf(input_file, "%d", &height);

    fscanf(input_file, "%d", &colors);


    #ifdef DEBUG
    printf("%s\n", file_format);
    printf("Width: %d, height: %d\n", width, height);
    printf("Colors: %d\n", colors);
    #endif

    /* Pobranie obrazu */
    picture = safe_malloc(height*sizeof(uint8_t*));
    result_picture = safe_malloc(height*sizeof(uint8_t*));
    for(int i=0; i<height; i++){
        picture[i] = safe_malloc(width*sizeof(uint8_t));
        result_picture[i] = safe_malloc(width*sizeof(uint8_t));
    }

    int scanf_int;
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            fscanf(input_file, "%d", &scanf_int);
            picture[i][j] = scanf_int;
        }
    }


    /* Usuwanie pliku tymczasowego */
    fclose(input_file);
    unlink(".temp");







    /* Otwarcie pliku filtra i zczytanie jego wlasciwosci */
    FILE *filter_file = fopen(filter_name, "r");
    if(filter_file == NULL){perror("Cannot open filter file");exit(1);}

    fscanf(filter_file, "%d", &filter_size);

    /* Wczytanie wartosci filtra */
    filter = safe_malloc(filter_size*sizeof(double*));
    for(int i=0; i<filter_size; i++){
        filter[i] = safe_malloc(filter_size*sizeof(double));
    }

    for(int i=0; i<filter_size; i++){
        for(int j=0; j<filter_size; j++){
            fscanf(filter_file, "%lf", &filter[i][j]);
        }
    }

    fclose(filter_file);

    #ifdef DEBUG
    printf("Filter size: %d\n", filter_size);
    for(int i=0; i<filter_size; i++){
        for(int j=0; j<filter_size; j++) printf("%lf ", filter[i][j]);
        printf("\n");
    }
    #endif



    /* Poczatek pomiaru czasu */
    double czas = pobierz_sekundy();

    /* Tablice na ID watkow i argumenty dla nich */
    pthread_t *thread_id = safe_malloc(threads_no*sizeof(thread_id));
    int *arg = safe_malloc(threads_no*sizeof(int));

    /* Inicjalizacja atrybutow */
    pthread_attr_t attr;
    if(pthread_attr_init(&attr) != 0){perror("Cannot init attr");exit(1);}

    /* Dodatkowo wyniki (zeby nie liczyc czasu na printf) */
    double **results = safe_malloc(threads_no*sizeof(double*));
    
    /* Tworzenie watkow */
    for(int i=0; i<threads_no; i++){
        arg[i] = i+1;
        
        /* Sprawdzanie czy kazdy watek otrzymuje odpowiednia czesc zadania */
        #ifdef DEBUG
        arg[i] = 1;
        #endif

        if(pthread_create(&thread_id[i], &attr, thread_routine, &arg[i]) != 0){perror("Cannot create thread");exit(1);}
        
        #ifdef DEBUG
        printf("[MAIN] Created thread: %lu\n", thread_id[i]);
        #endif
    }

    /* Czekanie na watki */
    for(int i=0; i< threads_no; i++){
        pthread_join(thread_id[i], &(((void**)results)[i]) );
        //printf("[MAIN] Watek %lu zakonczyl po %lf s\n", thread_id[i], *results[i]);
    }


    /* TODO: Koniec pomiaru czasu */
    czas = pobierz_sekundy() - czas;

    for(int i=0; i<threads_no; i++){
        printf("[MAIN] Watek %lu zakonczyl po %lf s\n", thread_id[i], *results[i]);
    }

    printf("\n[MAIN] Filtrowanie trwalo %lf s\n", czas);

    /* Filtrowanie wlasciwe */
    /*
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            generate_pixel(i,j);
        }
    }
    */



    /* Zapis wyniku */
    FILE *output_file = fopen(output_name, "w");
    if(input_file == NULL){perror("Cannot open output file");exit(1);}
    fprintf(output_file, "P2\n%d %d\n%d", width, height, colors);
    unsigned int written_values = 0;
    for(int i=0; i< height; i++){
        for(int j=0; j<width; j++){
            scanf_int = result_picture[i][j];
            if(written_values%10 == 0){
                fprintf(output_file, "\n");
            }else{
                fprintf(output_file, " ");
            }
            fprintf(output_file, "%d", scanf_int);
            written_values++;
        }
    }
    fprintf(output_file, "\n");

    fclose(output_file);


    /* Czyszczenie pamieci */
    for(int i=0; i<height; i++) free(picture[i]);
    free(picture);

    for(int i=0; i<height; i++) free(result_picture[i]);
    free(result_picture);

    for(int i=0; i<filter_size; i++) free(filter[i]);
    free(filter);

    for(int i=0; i<threads_no; i++){
        free(results[i]);
    }
    free(results);

    free(thread_id);
    free(arg);


    return 0;
}