#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<string.h>
#include<ctype.h>

#define COPY_MEM 0
#define COPY_CP 1

int copy_type;
int whole_time = 0;

void get_filename(char *path, char *buff, time_t mtime){
    int len = strlen(path);
    path += len;
    while(*path != '/') path--;
    path++;
    strcpy(buff, "archiwum/");
    strcat(buff, path);
    char *time_str = calloc(25, sizeof(char));
    strftime(time_str, 25, "_%Y-%m-%d_%H-%M-%S", localtime(&mtime));
    strcat(buff, time_str);
    free(time_str);
}

int isNum(char* str){
    while(*str != 0){
        if(isdigit(*str) == 0) return 0;
        str++;
    }
    return 1;
}
void trim_slash(char *path){
    //usun ewentualny '/' na końcu ścieżki
    while(*path != 0) path++;
    path--;
    if(*path == '/') *path = 0;
}

void error(char *msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

void monitor(char *path, int seconds){
    char *file_content = NULL;
    int length = 0;

    int changes = 0;    //liczba zmian w danym procesie
    char *absolute_path = calloc(200, sizeof(char));
    if(path[0] == '/'){
        trim_slash(path);
        absolute_path = strcpy(path, absolute_path);
    }
    else{
        char *realpath_result = realpath(path, absolute_path);
        if(realpath_result == NULL){
            printf("error: %s\n", path);
            error("cannot monitor file");
        }
    }
    //sciezka w absolute_path


    if(copy_type == COPY_MEM){
        //tresc pliku do pamieci
        FILE *fp = fopen(absolute_path, "r");
        fseek(fp, 0L, SEEK_END);
        length = ftell(fp);
        fseek(fp, 0L, SEEK_SET); //powrot
        file_content = calloc(length, sizeof(char));
        if(fread(file_content, 1, length, fp) != length) error("cannot read file");
        fclose(fp);
    }
    time_t mtime;
    struct stat stat_buff;
    stat(absolute_path, &stat_buff);
    mtime = stat_buff.st_mtime;
    while(whole_time >= seconds){
        sleep(seconds);
        whole_time -= seconds;

        stat(absolute_path, &stat_buff);
        if(stat_buff.st_mtime != mtime){
            printf("CHANGED!!!\n");
            changes++;
            mtime = stat_buff.st_mtime;
            if(copy_type == COPY_MEM){
                //utworzenie kopii z czasem
                char *filename = calloc(50, sizeof(char));
                get_filename(absolute_path, filename, mtime);
                FILE *fp = fopen(filename, "w");
                //printf("tworzenie: %s\n", filename);
                if(fwrite(file_content, 1, length, fp) != length) error("cannot write to file");
                fclose(fp);
                free(file_content);
                free(filename);
                
                fp = fopen(absolute_path, "r");
                fseek(fp, 0L, SEEK_END);
                length = ftell(fp);
                fseek(fp, 0L, SEEK_SET);
                file_content = calloc(length, sizeof(char));
                if(fread(file_content, 1, length, fp) != length) error("cannot read file");
                fclose(fp);
            }
            else if(copy_type == COPY_CP){
                //utworzenie kopii z czasem przy pomocy fork -> execlp
                if(vfork() == 0){
                    //potomek
                    char *filename = calloc(50, sizeof(char));
                    get_filename(absolute_path, filename, mtime);
                    execlp("cp", "cp", absolute_path, filename, NULL);
                    free(filename);
                }
            }
        }
    }
    free(absolute_path);
    exit(changes);
}

int main(int argc, char *argv[]){
    pid_t *children = calloc(100, sizeof(pid_t));
    int children_no = 0;
    whole_time = 10;

    if(argc < 4) error("wrong arguments");
    FILE *lista = fopen(argv[1], "r");
    if(lista == NULL) error("cannot open file");
		if(isNum(argv[2])) whole_time = atoi(argv[2]);
		else error("wrong arguments");

		if(strcmp(argv[3], "mem") == 0) copy_type = COPY_MEM;
		else if(strcmp(argv[3], "cp") == 0) copy_type = COPY_CP;
		else error("wrong arguments");
		
    char *path_buff = calloc(200, sizeof(char));
    int seconds;
    while(fscanf(lista, "%s %d\n", path_buff, &seconds) > 0){
        pid_t pid = fork();
        if(pid == 0){
            monitor(path_buff, seconds);
        }
        else{
            children[children_no++] = pid;
        }
        //printf("plik: %s, co %d sekund \n", path_buff, seconds);
    }


    while(children_no > 0){
        int status;
        pid_t which = wait(&status);
        status >>= 8;
        printf("PID %d: %d changes\n", which, status);
        children_no--;
    }

    free(path_buff);
    fclose(lista);
    return 0;
}
