﻿#include "my_library.h"
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

char** ptr_array = NULL;
char* current_dir = NULL;
char* current_file = NULL;
char* temp_file = NULL;
int array_size;
int iterator;

void init_array(int size) {
	array_size = size;
	ptr_array = (char**)calloc(size, sizeof(char*));
	iterator = 0;
}

void set_dir(char* dir) {
	current_dir = dir;
}

void set_file(char* file) {
	current_file = file;
}

void set_temp(char *temp) {
	temp_file = temp;
}

void search() {
	char *query = calloc(100, sizeof(char));
	strcpy(query, "find ");
	strcat(query, current_dir);
	strcat(query, " -name ");
	strcat(query, current_file);
	strcat(query, " > ");
	strcat(query, temp_file);
	system(query);
	free(query);
}

int reserve_block() {
	FILE *source = fopen(temp_file, "r"); //tylko do odczytu
	if (source == NULL) {
		printf("Cannot open temp file\n");
		exit(1);
	}

	fseek(source, 0L, SEEK_END);	//przemieszczenie do końca pliku
	int size = ftell(source);		//odczytanie pozycji - czyli długości pliku
	fseek(source, 0L, SEEK_SET);	//powrót na początek

	char *buffer = (char*)calloc(size, sizeof(char));	//alokacja bloku pamięci na wynik polecenia find
	fread(buffer, sizeof(char), size, source);
	fclose(source);					//zamknięcie pliku


	if (iterator == array_size) {
		printf("The array is full\n");
		exit(1);
	}

	ptr_array[iterator] = buffer;	//umieszczenie wskaźnika do bloku w tablicy

	return iterator++;
}

void erase_block(int index) {
	if (index >= iterator || ptr_array[index] == NULL) {
		printf("Cannot erase block: block at given index %d does not exist\n", index);
		exit(1);
	}
	free(ptr_array[index]);
	ptr_array[index] = NULL;	//zapobieganie użyciu free() dwa razy na tym samym wskaźniku
}

void free_array() {
	for (int i = 0; i < iterator; i++) {
		if (ptr_array[i] != NULL) free(ptr_array);
	}
	free(ptr_array);
}