﻿#include "my_library.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<time.h>

#include <unistd.h> 
#include<sys/times.h>

clock_t last_user;
clock_t last_kernel;
long long last_real;
FILE *raport = NULL;

void startMeas() {
	struct tms *time_buff = (struct tms*)calloc(1, sizeof(struct tms));
	times(time_buff);
	last_user = time_buff->tms_utime + time_buff->tms_cutime;	// sumowanie czasu procesu i jego potomków
	last_kernel = time_buff->tms_stime + time_buff->tms_cstime;	// jak wyżej
	free(time_buff);

	struct timespec *tp_buff = (struct timespec*)calloc(1, sizeof(struct timespec));
	clock_gettime(CLOCK_REALTIME, tp_buff);	//pobranie rzeczywistego czasu
	last_real = tp_buff->tv_sec*1000000000 + tp_buff->tv_nsec;	//rzeczywisty czas w nanosekundach
	free(tp_buff);
}

void stopMeas(char *purpose) {
	struct tms *time_buff = (struct tms*)calloc(1, sizeof(struct tms));
	times(time_buff);
	last_user = time_buff->tms_utime + time_buff->tms_cutime - last_user;
	last_kernel = time_buff->tms_stime + time_buff->tms_cstime - last_kernel;
	free(time_buff);

	struct timespec *tp_buff = (struct timespec*)calloc(1, sizeof(struct timespec));
	clock_gettime(CLOCK_REALTIME, tp_buff);
	last_real = tp_buff->tv_sec * 1000000000 + tp_buff->tv_nsec - last_real;
	free(tp_buff);

	last_user = 1;
	printf("Time measurement for %s:\n", purpose);
	printf("nanos passed: %Ld\n", last_real);
	printf("nanos in user mode: %ld\n", 1000000000 * last_user / sysconf(_SC_CLK_TCK));
	printf("nanos in system mode: %ld\n\n", 1000000000 * last_kernel / sysconf(_SC_CLK_TCK));

	//zapis do pliku
	fprintf(raport, "Time measurement for %s:\n", purpose);
	fprintf(raport, "nanos passed: %Ld\n", last_real);
	fprintf(raport, "nanos in user mode: %ld\n", 1000000000 * last_user / sysconf(_SC_CLK_TCK));
	fprintf(raport, "nanos in system mode: %ld\n\n", 1000000000 * last_kernel / sysconf(_SC_CLK_TCK));
}


void performSearch(char *directory, char *file, char *temp_file) {
	set_dir(directory);
	set_file(file);
	set_temp(temp_file);

	//pomiar czasu przeszukiwania katalogu
	startMeas();
	search();
	stopMeas("directory search"); //zakończ pomiar czasu

	//pomiar czasu rezerwacji bloku
	startMeas();
	reserve_block();
	stopMeas("block reservation");

}

void performRemovingIndex(int index) {
	startMeas();
	erase_block(index);
	stopMeas("removing block");
}

void wrongArguments() {
	printf("Wrong arguments\n");
	if (raport != NULL) fclose(raport);
	exit(1);
}

int isnum(char* string) {
	for (int i = 0; i < strlen(string); i++) {
		if (isdigit(string[i]) == 0) return 0;
	}
	return 1;
}

int main(int argc, char *argv[]) {
	

	if (argc < 3) {
		//brak przynajmniej 2 argumentów (create_table rozmiar)
		wrongArguments();
	}

	raport = fopen("raport2.txt", "w");

	if (raport == NULL) {
		printf("Cannot create raport file\n");
		exit(1);
	}

	//pierwsze dwa argumenty to musi być utworzenie tablicy
	int argi = 1;
	if (strcmp(argv[argi], "create_table") == 0) {
		//sprawdzenie czy następny argument jest numerem
		argi++;	//na pewno istnieje (argc >= 3)
		if (isnum(argv[argi]) != 0) {
			//argument jest liczbą całkowitą nieujemną - jest to rozmiar tablicy
			int size = atoi(argv[argi]);
			init_array(size);
		}
		else wrongArguments();
		argi++;
	}
	else wrongArguments();

	//parsowanie kolejnych argumentów
	while (argi < argc) {
		if (strcmp(argv[argi], "search_directory") == 0) {
			if (argc - argi <= 3) wrongArguments(); //powinny być jeszcze trzy argumenty (dir, file, temp_file)
			argi++;
			performSearch(argv[argi], argv[argi + 1], argv[argi + 2]);
			argi += 3;
		}
		else if (strcmp(argv[argi], "remove_block") == 0) {
			if (argc - argi <= 1) wrongArguments(); //powinien być jeszcze jeden argument (index)
			argi++;
			if (isnum(argv[argi]) == 0) wrongArguments();
			performRemovingIndex( atoi(argv[argi]) );
			argi++;
		}
		else wrongArguments();
	}

	//czyszczenie pamięci
	free_array();

	//zamykanie pliku z raportem
	fclose(raport);
	return 0;
}