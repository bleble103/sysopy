#pragma once

extern void init_array(int);
extern void set_dir(char*);
extern void set_file(char*);
extern void set_temp(char*);
extern void search(void);
extern int reserve_block(void);
extern void erase_block(int);
extern void free_array(void);

