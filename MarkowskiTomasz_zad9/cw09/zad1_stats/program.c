#define _XOPEN_SOURCE 750
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<time.h>


void sleepRandom(){
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = (rand()%15 + 1)*1e6;
    nanosleep(&ts, NULL);
}

double get_seconds(){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + 1e-9*ts.tv_nsec;
}

/* Mutexy */
pthread_mutex_t sem_in = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sem_in_ok = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sem_out = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sem_out_ok = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sem_start = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sem_start_ok = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t sem_stacja = PTHREAD_MUTEX_INITIALIZER; //robi przy okazji za mutex warunku
pthread_cond_t cond_out = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_in = PTHREAD_COND_INITIALIZER;

/* Do ktorej kolejki aktualnie sie wchodzi */
int in = 0;

/* Z ktorej kolejki aktualnie sie wychodzi */
int out = -1; //haxy

int passengers_no;
int wagons_no;
int capacity;
int rounds_no;

/* Wzgledny punkt w czasie dla znacznika czasu */
double ref_time;

/* Koniec programu */
int koncowa = 0;

/* Wolne miejsca w wagonikach i przyciski START */
int *free_places;
int *starts;
int *stats;

void *wagonik(void *arg){
    int w_id = *(int*)arg;
    int rounds_left = rounds_no;
    int first_time = 1;

    while(rounds_left > 0){
        if(first_time) pthread_mutex_lock(&sem_stacja);
        while(in != w_id){
            pthread_cond_wait(&cond_in, &sem_stacja);
        }

        if(first_time){
            printf("\n[%02d] [%.4lf] Otwieranie drzwi\n", w_id, get_seconds()-ref_time);
            if(w_id == wagons_no-1){
                out++;
            }
            first_time = 0;
        }
        while(free_places[w_id] > 0){
            pthread_mutex_unlock(&sem_in);
            pthread_mutex_lock(&sem_in_ok);
            //printf("[%d] Ktos wsiadl\n", w_id);
            free_places[w_id]--;
        }


        /* kwestia przycisku START */
        starts[w_id] = 0;
        for(int i=0; i<capacity; i++){
            pthread_mutex_unlock(&sem_start);
            pthread_mutex_lock(&sem_start_ok);
        }


        printf("[%02d] [%.4lf] Zamykanie drzwi\n",w_id, get_seconds()-ref_time);
        
        /* Nastepny moze wpuszczac */
        in++;
        if(in >= wagons_no) in = 0;
        pthread_cond_broadcast(&cond_in);


        printf("[%02d] [%.4lf] rozpoczecie jazdy\n", w_id, get_seconds()-ref_time);
        pthread_cond_broadcast(&cond_out);
        pthread_mutex_unlock(&sem_stacja);
        sleepRandom();
        pthread_mutex_lock(&sem_stacja);
        while(out != w_id){
            pthread_cond_wait(&cond_out, &sem_stacja);
        }
        printf("\n[%02d] [%.4lf] zakonczenie jazdy\n", w_id, get_seconds()-ref_time);
        printf("[%02d] [%.4lf] Otwieranie drzwi\n",w_id, get_seconds()-ref_time);
        while(free_places[w_id] < capacity){
            pthread_mutex_unlock(&sem_out);
            pthread_mutex_lock(&sem_out_ok);
            //printf("[%d] Ktos wysiadl\n", w_id);
            free_places[w_id]++;
        }
        rounds_left--;
        out++;
        if(out >= wagons_no){
            out = 0;
        }

       
    }
    

    printf("[%02d] [%.4lf] Koncze prace\n", w_id, get_seconds()-ref_time);
    pthread_cond_broadcast(&cond_out);
    pthread_mutex_unlock(&sem_stacja);

    return NULL;
}

void *pasazer(void *arg){
    int p_id = *(int*)arg;
    
    /* W ktorym jest wagoniku */
    int in_which;
    //printf("pasazer %d created\n", p_id);

    while(1){
        pthread_mutex_lock(&sem_in);
        
        /* Czy koncowa? */
        if(koncowa == 1){
            printf("\t[%02d] [%.4lf] koncze prace\n", p_id, get_seconds()-ref_time);
            pthread_mutex_unlock(&sem_in);
            pthread_exit(0);
        }

        in_which = in;
        stats[p_id]++;
        printf("\t+[%02d] [%.4lf] wsiadlem, w wagoniku: %d\n", p_id, get_seconds()-ref_time, capacity-free_places[in_which]+1);
        pthread_mutex_unlock(&sem_in_ok);


        /* Przycisk START */
        pthread_mutex_lock(&sem_start);
        if(starts[in_which] == 0){
            starts[in_which] = 1;
            printf("\t*[%02d] [%.4lf] wcisnalem START\n", p_id, get_seconds()-ref_time);

        }
        pthread_mutex_unlock(&sem_start_ok);


        pthread_mutex_lock(&sem_out);
        while(out != in_which){
            pthread_cond_wait(&cond_out, &sem_out);
        }
        printf("\t-[%02d] [%.4lf] wysiadlem, w wagoniku: %d\n", p_id, get_seconds()-ref_time, capacity-free_places[in_which]-1);
        pthread_mutex_unlock(&sem_out_ok);

    }

    return NULL;
}


int main(int argc, char *argv[]){

    if(argc != 5){printf("Args: passengers, wagons, capacity, rounds\n");exit(1);}
    passengers_no = atoi(argv[1]);
    wagons_no = atoi(argv[2]);
    capacity = atoi(argv[3]);
    rounds_no = atoi(argv[4]);

    srand(time(NULL));

    /* Pobranie czasu referencyjnego */
    ref_time = get_seconds();

    pthread_t *passengers_tid = malloc(passengers_no*sizeof(pthread_t));
    pthread_t *wagons_tid = malloc(wagons_no*sizeof(pthread_t));
    int *passengers_args = malloc(passengers_no*sizeof(int));
    int *wagons_args = malloc(wagons_no*sizeof(int));

    free_places = malloc(wagons_no*sizeof(int));
    starts = malloc(wagons_no*sizeof(int));
    stats = malloc(passengers_no*sizeof(int));
    for(int i=0; i<wagons_no; i++){
        free_places[i] = capacity;
        starts[i] = 0;
        stats[i] = 0;
    }

    /* Inicjalizacja wartosci mutexow */
    pthread_mutex_lock(&sem_in);
    pthread_mutex_lock(&sem_out);
    pthread_mutex_lock(&sem_in_ok);
    pthread_mutex_lock(&sem_out_ok);
    pthread_mutex_lock(&sem_start);
    pthread_mutex_lock(&sem_start_ok);
    
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    for(int i=0; i<wagons_no; i++){
        wagons_args[i] = i;
        if(pthread_create(&wagons_tid[i], &attr, wagonik, &wagons_args[i]) != 0){perror("Cannot create wagon thread");exit(1);}
    }

    for(int i=0; i<passengers_no; i++){
        passengers_args[i] = i;
        if(pthread_create(&passengers_tid[i], &attr, pasazer, &passengers_args[i]) != 0){perror("Cannot create wagon thread");exit(1);}
    }

    pthread_attr_destroy(&attr);


    void *retval;

    for(int i=0; i<wagons_no; i++){
        pthread_join(wagons_tid[i], &retval);
    }

    koncowa = 1;
    pthread_mutex_unlock(&sem_in);

    for(int i=0; i<passengers_no; i++){
        pthread_join(passengers_tid[i], &retval);
    }
    printf("\nSTATYSTYKI:\n");
    for(int i=0; i<passengers_no; i++){
        printf("Pasazer %02d: %d\n", i, stats[i]);
    }

    /* Zwalnianie pamieci */
    free(passengers_tid);
    free(wagons_tid);
    free(passengers_args);
    free(wagons_args);
    free(free_places);
    free(starts);
    free(stats);

    return 0;
}